#Wandershot
Wandershot is a smartphone app which helps travelers and sightseers to find places of interest.   
For achieving this, so-called �Snapspot� are defined which represent points of interest as a circular area.  
Inside of a Snapspot the user is asked to leave behind a photo for following travelers. These pictures are only shown if the other user is also located inside the same geographical area where they were made.   
The user can later look up which Snapspots he/she already visited and view all created pictures. New Snapspots can be found by either looking them up on the in-app Snapspot map or by getting close enough to get a proximity notification.  

This project was created by   
1. Michael Rieger: https://www.codingtub.eu/  
2. Martin Schneglberger: http://www.debugged.at  

during the winter semester 2016/17.  

The is also an iOS beta version and a web application to create Snapspots available.