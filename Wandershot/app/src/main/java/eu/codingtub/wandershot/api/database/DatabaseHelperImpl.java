package eu.codingtub.wandershot.api.database;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;

import eu.codingtub.wandershot.api.listener.OnSnapUploadListener;
import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.api.listener.OnUserDataListener;
import eu.codingtub.wandershot.api.listener.OnWandershotUsersReceivedListener;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.models.WandershotUser;
import eu.codingtub.wandershot.utils.FirebaseConstants;

/**
 * Contains the implementation of the methods defined in the interface {@link DatabaseHelper}.
 *
 * @author Michael Rieger
 * @author Martin Schneglberger
 * @see eu.codingtub.wandershot.api.database.DatabaseHelper
 */

public class DatabaseHelperImpl implements DatabaseHelper {

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mUserDatabaseRef;
    private DatabaseReference mSnapspotDatabaseRef;
    private FirebaseStorage mFirebaseStorage;

    /**
     * Constructor
     *
     * @param _firebaseDatabase the application's FirebaseDatabase instance
     * @see com.google.firebase.database.FirebaseDatabase
     */
    public DatabaseHelperImpl(FirebaseDatabase _firebaseDatabase, FirebaseStorage _firebaseStorage) {
        mFirebaseDatabase = _firebaseDatabase;
        mFirebaseStorage = _firebaseStorage;
    }

    /**
     * Initializes a user object represented by the following attributes
     *
     * @param _currentUser        the current user as FirebaseUser
     * @param _name               the user's name as String
     * @param _email              the user's email address as String
     * @param _country            the user's country as String
     * @param _birthday           the user's birth date as String
     * @param _profilePicturePath the path of the user's profile picture on the phone as String
     * @see java.lang.String
     * @see com.google.firebase.auth.FirebaseUser
     */
    @Override
    public void initUser(FirebaseUser _currentUser, String _name, String _email, String _country,
                         String _birthday, String _profilePicturePath, boolean _isGoogleSignIn) {
        // create new WandershotUser
        WandershotUser user = new WandershotUser(_currentUser.getUid(), _name, _email, _country, _birthday);

        // get the database reference
        mUserDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_USERS);

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(_name)
                .build();

        _currentUser.updateProfile(profileUpdates);

        if (_isGoogleSignIn) {
            profileUpdates = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(Uri.fromFile(new File(_profilePicturePath)))
                    .build();

            _currentUser.updateProfile(profileUpdates);
            mUserDatabaseRef.child(_currentUser.getUid()).child("name").setValue(_name);
            mUserDatabaseRef.child(_currentUser.getUid()).child("email").setValue(_email);
            mUserDatabaseRef.child(_currentUser.getUid()).child("userId").setValue(_currentUser.getUid());
            mUserDatabaseRef.child(_currentUser.getUid()).child("birthday").setValue("N/A");
            mUserDatabaseRef.child(_currentUser.getUid()).child("country").setValue("N/A");
        } else {
            mUserDatabaseRef.child(_currentUser.getUid()).setValue(user);
            uploadProfileImage(_currentUser, _profilePicturePath);
        }
    }

    /**
     * Uploads the user's profile picture from the given file path
     *
     * @param _currentUser the current user as FirebaseUser
     * @param _path        the path of a picture on the phone as String
     * @see java.lang.String
     * @see com.google.firebase.auth.FirebaseUser
     */
    @Override
    public void uploadProfileImage(final FirebaseUser _currentUser, String _path) {
        if (_path == null) return;      //default image is used

        // get the database reference
        String path = FirebaseConstants.STORAGE_PROFILE_PICTURE + "/" + _currentUser.getUid();
        StorageReference storageRef = mFirebaseStorage.getReferenceFromUrl(FirebaseConstants.STORAGE_REFERENCE);
        final StorageReference imageRef = storageRef.child(path);

        final Uri file = Uri.fromFile(new File(_path));

        UploadTask uploadTask = imageRef.putFile(file);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e("Upload", "i failed you");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setPhotoUri(uri)
                                .build();

                        _currentUser.updateProfile(profileUpdates);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Failure", e.toString());
                    }
                });
            }
        });
    }

    /**
     * Uploads the user's profile picture from the given file path
     *
     * @param _currentUser the current user as FirebaseUser
     * @param _path        the path of a picture on the phone as String
     * @param _snapspot    the Snapspot where the snap was taken
     * @param _listener    the callback listener
     * @see java.lang.String
     * @see com.google.firebase.auth.FirebaseUser
     * @see eu.codingtub.wandershot.models.Snapspot
     * @see OnSnapUploadListener
     */
    @Override
    public void uploadSnap(final FirebaseUser _currentUser, String _path, final Snapspot _snapspot, final OnSnapUploadListener _listener) {
        String path = FirebaseConstants.STORAGE_SNAPS + "/" + _snapspot.getID() + "/" + _currentUser.getUid();
        StorageReference storageRef = mFirebaseStorage.getReferenceFromUrl(FirebaseConstants.STORAGE_REFERENCE);
        StorageReference imageRef = storageRef.child(path);

        // get the database reference
        mUserDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_USERS);
        mSnapspotDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_SNAPSPOTS);

        final Uri file = Uri.fromFile(new File(_path));

        // set the metadata
/*
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("image/jpg")
                .setCustomMetadata(FirebaseConstants.META_USERNAME, _currentUser.getDisplayName())
                .setCustomMetadata(FirebaseConstants.META_SNAPSPOTNAME, _snapspot.getName())
                .setCustomMetadata(FirebaseConstants.META_LATITUDE, String.valueOf(_snapspot.getLatitude()))
                .setCustomMetadata(FirebaseConstants.META_LONGITUDE, String.valueOf(_snapspot.getLongitude()))
                .build();
*/
        UploadTask uploadTask = imageRef.putFile(file/*, metadata*/);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e("Upload", "i failed you");
                _listener.OnSnapUploadFailed("Couldn't upload file");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // snap upload was successful
                // add the snapspot to the user
                mUserDatabaseRef.child(_currentUser.getUid())
                        .child(FirebaseConstants.DATABASE_SNAPSPOTS)
                        .child(String.valueOf(_snapspot.getID()))
                        .setValue(true);

                // add the user to the snapspots visitors
                mSnapspotDatabaseRef.child(String.valueOf(_snapspot.getID()))
                        .child("snaps")
                        .child(_currentUser.getUid())
                        .setValue(true);

                // Success - Return the snapspot
                _listener.onSnapUploaded(_snapspot);
            }
        });
    }

    /**
     * Fetches all data of a specific user.
     *
     * @param _currentUser the currently signed in user
     * @param _listener    the callback listener
     * @see com.google.firebase.auth.FirebaseUser
     * @see OnUserDataListener
     */
    @Override
    public void getUserData(final FirebaseUser _currentUser, final OnUserDataListener _listener) {
        mUserDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_USERS);
        String path = FirebaseConstants.STORAGE_PROFILE_PICTURE + "/" + _currentUser.getUid();
        StorageReference storageRef = mFirebaseStorage.getReferenceFromUrl(FirebaseConstants.STORAGE_REFERENCE);
        final StorageReference imageRef = storageRef.child(path);

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot _dataSnapshots) {
                // get the user data
                final WandershotUser user = _dataSnapshots.child(_currentUser.getUid()).getValue(WandershotUser.class);

                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        user.setProfilePicture(uri.toString());
                        // Success - return the user object
                        _listener.onUserDataReceived(user);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Failure", e.toString());
                        // Success - return the user object
                        _listener.onUserDataReceived(user);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError _databaseError) {
                // Error
                _listener.onUserDataReceiveFailed("Error receiving user data");
            }
        };

        mUserDatabaseRef.addListenerForSingleValueEvent(eventListener);
    }

    /**
     * Returns all children of the users snapspots property.
     *
     * @param _currentUser the currently signed in user
     * @param _listener    the callback listener
     * @see com.google.firebase.auth.FirebaseUser
     * @see OnSnapspotsReceivedListener
     */
    public void getUserSnapspots(final FirebaseUser _currentUser, final OnSnapspotsReceivedListener _listener) {
        mSnapspotDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_SNAPSPOTS);

        // Query all snapspots where an entry for #_currentUser exists
        Query query = mSnapspotDatabaseRef.orderByChild("snaps/" + _currentUser.getUid()).equalTo(true);
        query.keepSynced(true);

        final ArrayList<Snapspot> snapspots = new ArrayList<>();

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot _dataSnapshots) {
                for (DataSnapshot snapshot : _dataSnapshots.getChildren()) {
                    Snapspot temp = snapshot.getValue(Snapspot.class);
                    snapspots.add(temp);
                }
                // Success - return list
                _listener.onSnapspotsReceived(snapspots);
            }

            @Override
            public void onCancelled(DatabaseError _databaseError) {
                // Error
                _listener.onSnapspotsReceiveFailed();
            }
        };

        query.addListenerForSingleValueEvent(eventListener);
    }

    /**
     * Returns all children of the snapspot users property.
     *
     * @param _currentSnapspot the Snapspot the user is currently in
     * @param _listener        the callback listener
     * @see com.google.firebase.auth.FirebaseUser
     * @see OnSnapspotsReceivedListener
     */
    public void getSnapspotUsers(final Snapspot _currentSnapspot, final OnWandershotUsersReceivedListener
            _listener) {
        mUserDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_USERS);
        Query query = mUserDatabaseRef.orderByChild("snapspots/" + _currentSnapspot.getID()).equalTo
                (true);

        query.keepSynced(true);

        final ArrayList<WandershotUser> users = new ArrayList<>();

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot _dataSnapshots) {
                for (DataSnapshot snapshot : _dataSnapshots.getChildren()) {
                    WandershotUser temp = snapshot.getValue(WandershotUser.class);
                    users.add(temp);
                }
                _listener.onWandershotUsersReceived(users);
            }

            @Override
            public void onCancelled(DatabaseError _databaseError) {
                _listener.onWandershotUsersReceivedFailed();
            }
        };

        query.addListenerForSingleValueEvent(eventListener);
    }

    /**
     * Returns a ArrayList of WandershotUsers which uploaded a Snap inside a certain Snapspot, which
     * is referenced to by a StorageReference pointing to a snap of this snapspot
     *
     * @param _reference StorageReference to a image taken inside of a snapspot
     * @param _listener the callback listener
     *
     * @see StorageReference
     */
    public void getUserNameByReference(StorageReference _reference,
                                         final OnWandershotUsersReceivedListener _listener){
        String tempStr = _reference.toString();
        String[] tempArr = tempStr.split("/");
        String snapSpotId = tempArr[4];
        final String userId = tempArr[5];

        mUserDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_USERS);
        Query query = mUserDatabaseRef.orderByChild("snapspots/" + snapSpotId).equalTo
                (true);

        query.keepSynced(true);

        final ArrayList<WandershotUser> users = new ArrayList<>();

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot _dataSnapshots) {
                for (DataSnapshot snapshot : _dataSnapshots.getChildren()) {
                    WandershotUser temp = snapshot.getValue(WandershotUser.class);
                    if(temp.getUserId().equals(userId)){
                        users.add(temp);
                        //TODO: Return all users at once and map the right inside of the FeedAdapter
                        _listener.onWandershotUsersReceived(users);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError _databaseError) {
                _listener.onWandershotUsersReceivedFailed();
            }
        };

        query.addListenerForSingleValueEvent(eventListener);

    }
}
