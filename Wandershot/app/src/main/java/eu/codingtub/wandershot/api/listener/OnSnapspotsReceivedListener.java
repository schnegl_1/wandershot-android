package eu.codingtub.wandershot.api.listener;

import java.util.ArrayList;

import eu.codingtub.wandershot.models.Snapspot;

/**
 * This interface acts as a callback for methods fetching a list of Snapspots from the database.
 *
 * @author Martin Schneglberger
 * @author Michael Rieger
 * @see eu.codingtub.wandershot.api.database.DatabaseHelperImpl
 * @see Snapspot
 */

public interface OnSnapspotsReceivedListener {

    /**
     * Is called when an action was successful
     */
    void onSnapspotsReceived(ArrayList<Snapspot> _snapspots);

    /**
     * Is called when an action failed
     */
    void onSnapspotsReceiveFailed();
}
