package eu.codingtub.wandershot.ui.profile.usersnapspots;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.ArrayList;

import eu.codingtub.wandershot.models.Snapspot;

/**
 * The interface defining all view-methods of the UserSnapspotActivity.
 *
 * @author Michael Rieger
 */

public interface UserSnapspotView extends MvpView {

    /**
     * Places marker on the locations of the Snapspots that the user visited.
     *
     * @param _snapspots a list of snapspots
     * @see eu.codingtub.wandershot.models.Snapspot
     */
    void setMarkers(ArrayList<Snapspot> _snapspots);

    /**
     * Show that the receiving of the snapspots failed.
     */
    void showSnapSpotReceiveFailed();
}
