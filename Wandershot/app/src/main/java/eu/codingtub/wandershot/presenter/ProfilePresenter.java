package eu.codingtub.wandershot.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import eu.codingtub.wandershot.api.listener.OnUserDataListener;
import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.models.WandershotUser;
import eu.codingtub.wandershot.ui.profile.ProfileView;

/**
 * The presenter used for the profile fragment.
 *
 * @author Michael Rieger
 */

public class ProfilePresenter extends MvpBasePresenter<ProfileView> implements OnUserDataListener {
    private AuthenticationHelper mAuthenticationHelper;
    private DatabaseHelper mDatabaseHelper;

    public ProfilePresenter(AuthenticationHelper _authenticationHelper, DatabaseHelper _databaseHelper) {
        mAuthenticationHelper = _authenticationHelper;
        mDatabaseHelper = _databaseHelper;
    }

    public void fetchUserData() {
        mDatabaseHelper.getUserData(mAuthenticationHelper.getCurrentUser(), this);
    }

    @Override
    public void onUserDataReceived(WandershotUser _user) {
        getView().setUserData(_user);
        getView().saveUserDataToSharedPreferences(_user);
    }

    @Override
    public void onUserDataReceiveFailed(String _exception) {
        getView().showDataFetchError();
    }

    public void onSnapspotsVisitedClicked() {
        getView().startUserSnapspotsActivity();
    }
}
