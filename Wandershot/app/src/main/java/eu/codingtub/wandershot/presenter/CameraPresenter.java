package eu.codingtub.wandershot.presenter;

import android.media.Image;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.io.File;

import eu.codingtub.wandershot.api.camera.ImageSaver;
import eu.codingtub.wandershot.api.camera.OnImageSavedListener;
import eu.codingtub.wandershot.ui.camera.CameraView;

/**
 * The presenter used for the CameraActivity.
 * <p>
 * Implements an OnImageSavedListener for handling the saved picture.
 * </p>
 *
 * @author Michael Rieger
 * @see eu.codingtub.wandershot.api.camera.OnImageSavedListener
 */
public class CameraPresenter extends MvpBasePresenter<CameraView> implements OnImageSavedListener {

    /**
     * Handles the OnClick event of the take photo button. Takes a picture.
     */
    public void takePhotoButtonClicked() {
        getView().takePicture();
    }

    /**
     * Handles the OnClick event of the switch camera button. Switches the camera (back or front).
     */
    public void switchCameraClicked() {
        getView().switchCamera();
    }

    public Runnable savePicture(Image _image, File _file) {
        return new ImageSaver(_image, _file, this);
    }

    /**
     * Callback listener of the ImageSaver. Called when the image was saved successfully.
     * Advises the view to call the cropImage method.
     *
     * @param _file the saved File
     */
    @Override
    public void onImageSaved(File _file) {
        getView().cropImage(_file);
    }

    /**
     * Callback listener of the ImageSaver. Called could not be saved. Shows an error message on
     * the view.
     *
     * @param _exception the error that occurred while performing the action as String
     */
    @Override
    public void onImageSaveFailed(String _exception) {
        getView().showMessage(_exception);
    }
}
