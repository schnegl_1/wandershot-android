package eu.codingtub.wandershot.ui.registration;

import com.hannesdorfmann.mosby.mvp.MvpView;

import eu.codingtub.wandershot.utils.Error;

/**
 * Definitions of methods needed for the RegisterView.
 *
 * @author Michael Rieger
 */

public interface RegisterView extends MvpView {
    /**
     * Starts the MainActivity.
     */
    void startMainActivity();

    /**
     * Shows the user if an attribute was passed empty and has to be entered in the view's text.
     *
     * @param _error the error that occurred as Error
     * @see eu.codingtub.wandershot.utils.Error
     */
    void showAttributeError(Error _error);

    /**
     * Shows the user which error occurred while registration was in progress.
     *
     * @param _exception the message of the exception that occurred as String
     * @see java.lang.String
     */
    void showRegistrationError(String _exception);

    /**
     * Shows a DatePicker to let the user select the birthday.
     */
    void getBirthdayFromDialog();

    /**
     * Shows a CountryPicker to let the user choose the country.
     */
    void getCountryFromDialog();

    /**
     * Shows the user a gallery to pick a profile picture from.
     */
    void selectImageFromGallery();

    /**
     * Shows a ProgressDialog to the user.
     */
    void showProgressDialog();

    /**
     * Stops the previously started ProgressDialog.
     */
    void stopProgressDialog();

    /**
     * Navigates back to the LoginActivity.
     */
    void navigateUp();

    /**
     * Resets the apps SharedPreferences
     */
    void resetSharedPreferences();

}
