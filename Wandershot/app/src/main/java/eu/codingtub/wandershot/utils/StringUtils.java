package eu.codingtub.wandershot.utils;

/**
 * A class for specific actions that can be performed on Strings.
 *
 * @author Michael Rieger
 */

public class StringUtils {
    /**
     * Checks if a String is null or empty.
     *
     * @param _string the string that should be checked
     * @return true if the string was null or empty. false otherwise
     * @see java.lang.String
     */
    public static boolean isNullOrEmpty(String _string) {
        return _string == null || _string.isEmpty() || _string.trim().isEmpty();
    }

    /**
     * Checks if a series of Strings is null or empty.
     *
     * @param _string the string that should be checked
     * @return true if the string was null or empty. false otherwise
     * @see java.lang.String
     */
    public static boolean isNullOrEmpty(String... _string) {
        boolean isNullOrEmpty = false;

        for (String s : _string) {
            isNullOrEmpty = s == null || s.isEmpty() || s.trim().isEmpty();
        }

        return isNullOrEmpty;
    }

    /**
     * Checks a month represented by an integer and adds a leading zero if the month is less than 10.
     *
     * @param _month the month that should be checked
     * @return a String that contains the wanted formatted string
     */
    public static String checkDateLeadingZero(int _month) {
        StringBuilder sMonthBuilder = new StringBuilder();

        if (_month < 10) {
            sMonthBuilder.append("0").append(_month);
        } else {
            sMonthBuilder.append(_month);
        }

        return sMonthBuilder.toString();
    }
}
