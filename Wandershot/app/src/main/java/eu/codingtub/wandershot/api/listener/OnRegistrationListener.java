package eu.codingtub.wandershot.api.listener;

/**
 * This class acts as a callback listener used with database interaction, specifically designed for
 * use with the DatabaseHelper class.
 *
 * @author Michael Rieger
 * @see eu.codingtub.wandershot.api.database.DatabaseHelper
 */

public interface OnRegistrationListener {

    /**
     * Is called when an action was successful.
     * Parameters should later be handed over to the implemented method handling further database
     * related tasks.
     *
     * @param _name               the user's name as String
     * @param _email              the user's email address as String
     * @param _country            the user's country as String
     * @param _birthday           the user's birthday as String
     * @param _password           the password as String
     * @param _profilePicturePath the path of the user's profile picture as String
     * @see java.lang.String
     */
    void onUserRegistrated(String _name, String _email, String _country, String _birthday,
                           String _password, String _profilePicturePath);

    /**
     * Is called if the action performed failed
     *
     * @param _exception the error that occurred while performing the action
     * @see java.lang.String
     */
    void onUserRegistrationFailed(String _exception);
}
