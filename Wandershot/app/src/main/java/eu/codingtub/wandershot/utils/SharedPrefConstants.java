package eu.codingtub.wandershot.utils;

/**
 * The constant keys that are used for persisting some user data in SharedPreferences.
 *
 * @author Michael Rieger
 */

public final class SharedPrefConstants {

    public static final String KEY_USERID = "userId";

    public static final String KEY_NAME = "name";

    public static final String KEY_EMAIL = "email";

    public static final String KEY_COUNTRY = "country";

    public static final String KEY_BIRTHDAY = "birthday";

    public static final String KEY_PROFILE_PICTURE = "profile_picture";
}
