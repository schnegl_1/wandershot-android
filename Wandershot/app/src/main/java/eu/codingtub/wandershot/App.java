package eu.codingtub.wandershot;

import android.app.Application;
import android.app.PendingIntent;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.api.authentication.AuthenticationHelperImpl;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.api.database.DatabaseHelperImpl;
import eu.codingtub.wandershot.api.location.LocationHelper;
import eu.codingtub.wandershot.api.location.LocationHelperImpl;
import eu.codingtub.wandershot.ui.map.HomeFragment;

/**
 * This class is designed as a Singleton and contains an instance of an {@link AuthenticationHelper}
 * , a {@link DatabaseHelper} and a {@link LocationHelper}.
 *
 * @author Michael Rieger
 * @see android.app.Application
 * @see eu.codingtub.wandershot.api.authentication.AuthenticationHelper
 * @see eu.codingtub.wandershot.api.database.DatabaseHelperImpl
 * @see eu.codingtub.wandershot.api.location.LocationHelper
 */

public class App extends Application {
    private static App sInstance;

    private DatabaseHelper mDatabaseHelper;
    private AuthenticationHelper mAuthenticationHelper;
    private LocationHelper mLocationHelper;
    private PendingIntent mLocationUpdateIntent;

    private boolean mIsHomeFragmentInForeground;

    @Override
    public void onCreate() {
        super.onCreate();

        // Check if Firebase was initialized then set contexts
        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            sInstance = this;

            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            mDatabaseHelper = new DatabaseHelperImpl(firebaseDatabase, firebaseStorage);

            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            mAuthenticationHelper = new AuthenticationHelperImpl(firebaseAuth);

            mLocationHelper = new LocationHelperImpl(firebaseDatabase);
        }
    }

    /**
     * Get the applications instance. Always returns the same instance.
     *
     * @return the Application instance
     * @see android.app.Application
     */
    public static App get() {
        return sInstance;
    }

    /**
     * Getter for the AuthenticationHelper instance. Always returns the same instance.
     *
     * @return the application AuthenticationHelper instance
     * @see DatabaseHelper
     */
    public AuthenticationHelper getAuthenticationHelper() {
        return mAuthenticationHelper;
    }

    /**
     * Getter for the DatabaseHelper instance. Always returns the same instance.
     *
     * @return the application DatabaseHelper instance
     * @see DatabaseHelper
     */
    public DatabaseHelper getDatabaseHelper() {
        return mDatabaseHelper;
    }

    /**
     * Getter for the LocationHelper instance. Always returns the same instance.
     *
     * @return the application LocationImpl instance
     * @see LocationHelper
     */
    public LocationHelper getLocationHelper() {
        return mLocationHelper;
    }

    public boolean isHomeFragmentInForeground() {
        return mIsHomeFragmentInForeground;
    }

    public void setHomeFragmentInForeground(boolean _homeFragmentInForeground) {
        mIsHomeFragmentInForeground = _homeFragmentInForeground;
    }

    public PendingIntent getLocationUpdateIntent() {
        return mLocationUpdateIntent;
    }

    public void setLocationUpdateIntent(PendingIntent _locationUpdateIntent) {
        mLocationUpdateIntent = _locationUpdateIntent;
    }
}
