package eu.codingtub.wandershot.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.ui.main.MainView;

/**
 * The presenter used for the MainActivity.
 *
 * @author Michael Rieger
 */

public class MainPresenter extends MvpBasePresenter<MainView> {
    private AuthenticationHelper mAuthHelper;

    /**
     * Constructor
     *
     * @param _authenticationHelper the AuthenticationHelper instance
     * @see eu.codingtub.wandershot.api.authentication.AuthenticationHelper
     */
    public MainPresenter(AuthenticationHelper _authenticationHelper) {
        super();
        this.mAuthHelper = _authenticationHelper;
    }

    /**
     * Executes the views startLoginActivity method if the user is not signed in.
     */
    public void startLoginActivityIfNotLoggedIn() {
        if (!mAuthHelper.isUserSignedIn()) {
            getView().resetSharedPreferences();
            getView().startLoginActivity();
        }
    }

    /**
     * Logs out the user and starts the LoginActivity again.
     */
    public void signOut() {
        mAuthHelper.signOut();
        getView().resetSharedPreferences();
        startLoginActivityIfNotLoggedIn();
    }
}
