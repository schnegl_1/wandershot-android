package eu.codingtub.wandershot.ui.login;

import com.google.android.gms.common.api.GoogleApiClient;
import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Definitions of methods needed for the LoginView.
 *
 * @author Michael Rieger
 */

public interface LoginView extends MvpView {
    /**
     * Starts the MainActivity.
     */
    void startMainActivity();

    /**
     * Starts the RegisterActivity.
     */
    void startRegisterActivity();

    /**
     * Starts the Intent for choosing a Google Account for a sign in via Google.
     *
     * @param _googleApiClient the GoogleApiClient instance
     * @see com.google.android.gms.common.api.GoogleApiClient
     */
    void signInViaGoogle(GoogleApiClient _googleApiClient);

    /**
     * Shows the user that either the email or password field was left empty.
     */
    void showInputError();

    /**
     * Starts a ProgressDialog showing the user that a Login is being performed.
     */
    void showProgressDialog();

    /**
     * Stops a currently running ProgressDialog.
     */
    void stopProgressDialog();

    /**
     * Shows the user that the sign in process was successful and forward to the MainActivity.
     */
    void logInSuccessful();

    /**
     * Shows the user that the sign in process failed and displays an error message.
     *
     * @param _exception the message of the exception that was caught while signing in
     */
    void logInFailed(String _exception);
}
