package eu.codingtub.wandershot.ui.main;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Definitions of methods needed for the MainView.
 *
 * @author Michael Rieger
 */

public interface MainView extends MvpView {

    /**
     * Starts the LoginActivity and deletes the activity stack so signing in cannot be bypassed.
     */
    void startLoginActivity();

    /**
     * Resets the apps SharedPreferences
     */
    void resetSharedPreferences();
}
