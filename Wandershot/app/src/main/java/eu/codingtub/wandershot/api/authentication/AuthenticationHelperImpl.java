package eu.codingtub.wandershot.api.authentication;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.io.File;

import eu.codingtub.wandershot.api.listener.OnAuthenticationListener;
import eu.codingtub.wandershot.api.listener.OnGoogleSignInListener;
import eu.codingtub.wandershot.api.listener.OnRegistrationListener;

/**
 * Contains the implementation of the methods defined in the interface {@link AuthenticationHelper}.
 *
 * @author Michael Rieger
 * @see eu.codingtub.wandershot.api.authentication.AuthenticationHelper
 */

public class AuthenticationHelperImpl implements AuthenticationHelper {
    private final FirebaseAuth mFirebaseAuth;
    private GoogleApiClient mGoogleApiClient;

    /**
     * Constructor
     *
     * @param _firebaseAuth the application's FirebaseAuth instance
     * @see com.google.firebase.auth.FirebaseAuth
     */
    public AuthenticationHelperImpl(FirebaseAuth _firebaseAuth) {
        mFirebaseAuth = _firebaseAuth;
    }

    /**
     * Try to login the user at the database. Call method of {@link OnAuthenticationListener} according to
     * success state.
     *
     * @param _email    the email of the user as String
     * @param _password the password of the user as String
     * @param _listener the callback listener
     * @see java.lang.String
     * @see OnAuthenticationListener
     */
    @Override
    public void userSignIn(String _email, String _password, final OnAuthenticationListener _listener) {
        mFirebaseAuth.signInWithEmailAndPassword(_email, _password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> _task) {
                if (_task.isSuccessful()) {
                    FirebaseUser user = mFirebaseAuth.getCurrentUser();

                    if (user != null) {
                        _listener.onUserSignedIn();
                    }
                } else {
                    try {
                        throw _task.getException();
                    } catch (Exception e) {
                        // TODO: 30.12.2016 figure out how to handle AuthErrors correctly
                        _listener.onSignInFailed("");
                    }
                }
            }
        });
    }

    /**
     * Try to login the user via Google Sign in. Call method of {@link OnAuthenticationListener} according
     * to success state.
     *
     * @param _listener the callback listener
     * @param _result   the result of the sign in process
     * @see com.google.android.gms.auth.api.signin.GoogleSignInResult
     * @see OnAuthenticationListener
     */
    @Override
    public void googleSignIn(GoogleSignInResult _result, final OnGoogleSignInListener _listener) {
        if (_result.isSuccess()) {
            GoogleSignInAccount acc = _result.getSignInAccount();

            final String name = acc.getDisplayName();
            final String email = acc.getEmail();
            final String profilePicture = acc.getPhotoUrl().toString();

            final File profilePictureFile = new File(profilePicture);

            AuthCredential credential = GoogleAuthProvider.getCredential(acc.getIdToken(), null);
            mFirebaseAuth.signInWithCredential(credential).addOnCompleteListener(
                    new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = mFirebaseAuth.getCurrentUser();

                                if (user != null) {
                                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                            .setPhotoUri(Uri.fromFile(profilePictureFile))
                                            .build();

                                    user.updateProfile(profileUpdates);

                                    _listener.onGoogleSignInSuccess(name, email, "N/A", "N/A", profilePicture);
                                }
                            } else {
                                _listener.onGoogleSignInFailed("");
                            }
                        }
                    }
            );
        } else {
            _listener.onGoogleSignInFailed("");
        }
    }

    /**
     * Try to register the user at the database. Call method of {@link OnRegistrationListener} according
     * to success state.
     *
     * @param _email    the email of the user as String
     * @param _password the password of the user as String
     * @param _listener the callback listener
     * @see java.lang.String
     * @see OnRegistrationListener
     */
    @Override
    public void registerUser(final String _name, final String _email, final String _country,
                             final String _birthday, final String _password,
                             final String _profilePicturePath, final OnRegistrationListener _listener) {
        mFirebaseAuth.createUserWithEmailAndPassword(_email, _password).addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> _task) {
                        if (_task.isSuccessful()) {
                            _listener.onUserRegistrated(_name, _email, _country, _birthday, _password, _profilePicturePath);
                        } else {
                            try {
                                throw _task.getException();
                            } catch (Exception e) {
                                // TODO: 30.12.2016 figure out how to handle AuthErrors correctly
                                _listener.onUserRegistrationFailed("");
                            }
                        }
                    }
                }
        );
    }

    /**
     * Sign out the user from the database.
     */
    @Override
    public void signOut() {
        mFirebaseAuth.signOut();
    }

    /**
     * Checks if the user is currently signed in to the database.
     *
     * @return true if signed in, false otherwise
     * @see com.google.firebase.auth.FirebaseUser
     */
    @Override
    public boolean isUserSignedIn() {
        return mFirebaseAuth.getCurrentUser() != null;
    }

    /**
     * Returns the currently signed in user.
     *
     * @return the currently signed in user as {@link FirebaseUser}
     * @see com.google.firebase.auth.FirebaseUser
     */
    @Override
    public FirebaseUser getCurrentUser() {
        return mFirebaseAuth.getCurrentUser();
    }

    /**
     * Returns an instance of an GoogleApiClient.
     *
     * @return the instance of an GoogleApiClient
     * @see com.google.android.gms.common.api.GoogleApiClient
     */
    @Override
    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    /**
     * Sets the GoogleApiClient instance.
     *
     * @param _googleApiClient the instance of the GoogleApiClient
     * @see com.google.android.gms.common.api.GoogleApiClient
     */
    public void setGoogleApiClient(GoogleApiClient _googleApiClient) {
        mGoogleApiClient = _googleApiClient;
    }
}
