package eu.codingtub.wandershot.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.ui.main.MainActivity;
import eu.codingtub.wandershot.ui.map.HomeFragment;

/**
 * This DialogFragment notifies the user whenever he is inside a Snapspot and the HomeFragment is in
 * Foreground.
 * It shows two buttons, one to dismiss the dialog and one to open the camera or the Snapspot-Feed,
 * depending on if the user already uploaded a picture before.
 *
 * @author Martin Schneglberger
 * @see eu.codingtub.wandershot.ui.feed.FeedFragment
 * @see HomeFragment
 */

public class InsideSnapspotDialog extends DialogFragment {

    public final static String NAME_KEY = "name";
    public final static String UPLOADED_PICTURE_KEY = "uploadedPicture";

    /**
     * Creats a new instace of this Dialog
     *
     * @param _nameOfSnapspot      String containing the name of the snapspot the user is currently in
     * @param _userUploadedPicture boolean to distinguish between opening the feed or the camera
     * @return instance of InsideSnapspotDialog
     */
    public static InsideSnapspotDialog newInstance(String _nameOfSnapspot, boolean
            _userUploadedPicture) {
        InsideSnapspotDialog dialog = new InsideSnapspotDialog();
        Bundle args = new Bundle();
        args.putString(NAME_KEY, _nameOfSnapspot);
        args.putBoolean(UPLOADED_PICTURE_KEY, _userUploadedPicture);
        dialog.setArguments(args);
        return dialog;
    }

    /**
     * Gets called when the Dialog gets opened
     *
     * @param _savedInstanceState The last saved instance state of the Fragment, or null if this is
     *                            a freshly created Fragment.
     * @return A new Dialog instance to be displayed by the Fragment.
     */
    @Override
    public Dialog onCreateDialog(Bundle _savedInstanceState) {
        String nameOfSnapspot = getArguments().getString(NAME_KEY);
        String contentText = "You are inside the Snapspot " + nameOfSnapspot + "!";

        boolean userUploadedPicture = getArguments().getBoolean(UPLOADED_PICTURE_KEY);

        LayoutInflater inflator = getActivity().getLayoutInflater();
        View content = inflator.inflate(R.layout.dialog_inside_snapspot, null);

        ((TextView) content.findViewById(R.id.dialog_inside_snapspot_text)).setText(contentText);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (!userUploadedPicture) { //Dialog should open camera
            builder.setPositiveButton(R.string.open_camera, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface _dialogInterface, int _i) {
                    _dialogInterface.dismiss();
                    ((HomeFragment) getFragmentManager().findFragmentByTag(MainActivity.HOME_TAG))
                            .startCameraActivity();
                }
            })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface _dialogInterface, int _i) {
                            _dialogInterface.dismiss();
                        }
                    })
                    .setView(content);
        } else { //Dialog should open feed
            builder.setPositiveButton(R.string.map_open_feed, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface _dialogInterface, int _i) {
                    _dialogInterface.dismiss();
                    ((HomeFragment) getFragmentManager().findFragmentByTag(MainActivity.HOME_TAG))
                            .changeToFeedFragment();
                }
            })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface _dialogInterface, int _i) {
                            _dialogInterface.dismiss();
                        }
                    })
                    .setView(content);
        }

        return builder.create();
    }
}
