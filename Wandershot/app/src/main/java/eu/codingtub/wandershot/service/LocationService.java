package eu.codingtub.wandershot.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.api.location.LocationHelper;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.ui.main.MainActivity;
import eu.codingtub.wandershot.ui.map.HomeFragment;

/**
 * This service is responsible for fetching and providing LocationUpdates via the GoogleApi.
 * <p>
 * Every update is forwarded to the LocationHelper
 * </p>
 *
 * @author Martin Schneglberger
 * @see GoogleApiClient
 * @see LocationListener
 * @see eu.codingtub.wandershot.api.location.LocationHelperImpl
 */
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, DecideOnLocationCallback {

    private static final String TAG = "LocationService";
    private static final int LOCATION_SERVICE_NOTIFICATION = 9696;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient = null;
    boolean mSendLocationToFragement;
    boolean mShowDialogToUser = false;
    boolean mSkipNextDialog = false; //needed to prevent lagging when switching from feed to home
    Messenger mCallback = null;

    Location mLastLocation;

    /**
     * The onStartCommand for this Service
     *
     * @param _intent  Passed intent, should contain a Messenger to communicate with the
     *                 HomeFragment
     * @param _flags   Additional data about this start request
     * @param _startId A unique integer representing this specific request to start
     * @return Mode the service should run in
     * @see Messenger
     * @see HomeFragment
     */
    @Override
    public int onStartCommand(Intent _intent, int _flags, int _startId) {
        if (_intent != null) {
            mCallback = (Messenger) _intent.getParcelableExtra(HomeFragment.MESSENGER_KEY);
        }
        mSendLocationToFragement = true;

        if (App.get().getLocationHelper().getCurrentSnapspot() != null) {
            hideInsideSnapspotButton(false);
            sendMessageToFragment(HomeFragment.LOCATION_DATA_KEY, HomeFragment.LOCATION_KEY,
                    mLastLocation);
            mSendLocationToFragement = false;
            mShowDialogToUser=false;
            mSkipNextDialog=true;
        }

        return START_STICKY;
    }

    /**
     * The onCreate-method of this service.
     * <p>
     * Initializes the GoogleApiClient
     * </p>
     *
     * @see GoogleApiClient
     */
    @Override
    public void onCreate() {
        Log.d(TAG, "Service on start command has been triggered");

        if (isGooglePlayServicesAvailable() && mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
        super.onCreate();
    }

    /**
     * The onBind-method of the Service
     *
     * @param _intent Passed Intent
     * @return IBinder communication channel to this service
     * @see IBinder
     */
    @Nullable
    @Override
    public IBinder onBind(Intent _intent) {
        Log.d(TAG, "binded");
        return null;
    }

    /**
     * Checks if GooglePlayServices are available and up-to-date on this device
     *
     * @return true if available and up-to-date, false otherwise
     */
    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            Log.e(TAG, " ***** Update google play service ");
            return false;
        }
    }

    /**
     * Callback which gets invoked when the GoogleApiClient was successfully connected.
     * Checks if GPS is enabled afterwards.
     *
     * @param _bundle some data, that should be passed
     * @see GoogleApiClient
     */
    @Override
    public void onConnected(@Nullable Bundle _bundle) {
        startLocationUpdates();
    }

    /**
     * Initializes the LocationRequest with all properties set in the LocationHelper-interface
     *
     * @see LocationHelper
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        //Set maximal Interval which can pass before the Service requests an update itself
        //If a other app requests an update during this interval, the update is also passed to
        // this service
        mLocationRequest.setInterval(LocationHelper.LOCATION_CHECK_INTERVAL_MILLI);

        //Set the minimal Interval which has to pass before another update gets sent.
        //Is needed to give the service enough time to process its calculations
        mLocationRequest.setFastestInterval(LocationHelper.FASTEST_LOCATION_CHECK_INTERVAL_MILLI);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Requests the LocationService.FusedLocationApi to start LocationUpdates
     *
     * @see LocationService
     */
    private void startLocationUpdates() {
        createLocationRequest();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    /**
     * Gets called when the connection to the GoogleApiClient is lost
     *
     * @param _i int representing the cause of the error
     * @see GoogleApiClient
     */
    @Override
    public void onConnectionSuspended(int _i) {
        if (_i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        } else if (_i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Network lost. Please re-connect.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Gets called when the connection to the GoogleApiClient failed
     *
     * @param _connectionResult ConnectionResult representing the cause of the error
     * @see GoogleApiClient
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult _connectionResult) {

    }

    /**
     * Gets invoked whenever the a new Location is received
     *
     * @param _location Location object with the new user location
     * @see Location
     */
    @Override
    public void onLocationChanged(Location _location) {
        if (App.get().getLocationHelper().getSnapspotsList() != null) {
            //If-Else needed to prevent HomeFragment from lagging (zooming+showing dialog)
            if (mSendLocationToFragement) {
                sendMessageToFragment(HomeFragment.LOCATION_DATA_KEY, HomeFragment.LOCATION_KEY,
                        _location);
                mSendLocationToFragement = false;
            } else {
                App.get().getLocationHelper().onLocationChanged(_location, this);
            }
            mLastLocation = _location;
        } else {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            stopSelf();
        }
    }

    /**
     * Sends message to the HomeFragment containing a string
     *
     * @param _dataKey The data-Key for the Bundle
     * @param _what    The what-of the message
     * @param _data    String containing the data which should be sent
     * @see Messenger
     * @see HomeFragment
     */
    private void sendMessageToFragment(String _dataKey, int _what, String _data) {
        Bundle bundle = new Bundle();
        bundle.putString(_dataKey, _data);

        Message msg = Message.obtain();
        msg.what = _what;
        msg.setData(bundle);

        try {
            mCallback.send(msg);
        } catch (RemoteException _e) {
            Log.e(TAG, "Error sending message");
        }
    }

    /**
     * Sends message to the HomeFragment containing a boolean
     *
     * @param _dataKey The data-Key for the Bundle
     * @param _what    The what-of the message
     * @param _data    boolean containing the data which should be sent
     * @see Messenger
     * @see HomeFragment
     */
    private void sendMessageToFragment(String _dataKey, int _what, boolean _data) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(_dataKey, _data);

        Message msg = Message.obtain();
        msg.what = _what;
        msg.setData(bundle);

        try {
            mCallback.send(msg);
        } catch (RemoteException _e) {
            Log.e(TAG, "Error sending message");
        }
    }

    /**
     * Sends message to the HomeFragment containing a Location
     *
     * @param _dataKey The data-Key for the Bundle
     * @param _what    The what-of the message
     * @param _data    Location containing the data which should be sent
     * @see Messenger
     * @see HomeFragment
     */
    private void sendMessageToFragment(String _dataKey, int _what, Location _data) {

        Message msg = Message.obtain();
        msg.what = _what;
        msg.obj = _data;

        try {
            mCallback.send(msg);
        } catch (RemoteException _e) {
            Log.e(TAG, "Error sending message");
        }
    }

    /**
     * Sends a Message to the Fragment which triggers showing or hiding the Map-Button.
     * Also checks if the user uploaded a picture before
     *
     * @param _shouldHide boolean, true if the button should not be visible
     */
    @Override
    public void hideInsideSnapspotButton(boolean _shouldHide) {
        sendMessageToFragment(HomeFragment.BUTTON_STATE_DATA_KEY, HomeFragment.BUTTON_STATE_KEY,
                _shouldHide);
        if (!_shouldHide) {
            checkIfUserUploadedPictureBefore();
        }
    }

    /**
     * Checks if the has uploaded a picture inside of the current snapspot before
     *
     * @see DatabaseHelper
     * @see LocationHelper
     */
    private void checkIfUserUploadedPictureBefore() {
        DatabaseHelper databaseHelper = App.get().getDatabaseHelper();

        final ArrayList<Snapspot> userSnapspots = new ArrayList<>();

        databaseHelper.getUserSnapspots(FirebaseAuth.getInstance().getCurrentUser(), new
                OnSnapspotsReceivedListener() {
                    @Override
                    public void onSnapspotsReceived(ArrayList<Snapspot> _snapspots) {
                        userSnapspots.addAll(_snapspots);

                        Snapspot currentSnapspot = App.get().getLocationHelper()
                                .getCurrentSnapspot();
                        for (Snapspot spot : userSnapspots) {
                            if (spot.getID() == currentSnapspot.getID()) {
                                sendMessageToFragment(HomeFragment.UPLOADED_SNAP_DATA_KEY,
                                        HomeFragment
                                        .UPLOADED_SNAP_KEY, true);

                                break;
                            }
                        }

                        if(App.get().isHomeFragmentInForeground()&&mShowDialogToUser){
                            mShowDialogToUser=false;
                            sendMessageToFragment(HomeFragment.SNAPSPOT_DATA_KEY,
                                    HomeFragment.SNAPSPOT_KEY,
                                    App.get().getLocationHelper().getCurrentSnapspot()
                                            .getName());
                        }
                    }

                    @Override
                    public void onSnapspotsReceiveFailed() {
                        Log.e(TAG, "Couldn't get the users snapspots");
                    }
                });
    }

    /**
     * Gets called from the LocationHelperImpl if the user currently is inside of a Snapspot
     * Showes an Notification if the app runs in background or sets the boolean to show a
     * dialog on the checkIfUserUploadedPictureBefore-result.
     *
     * @param _snapspot Snapspot the user is in
     * @see Snapspot
     * @see eu.codingtub.wandershot.api.location.LocationHelperImpl
     * @see HomeFragment
     */
    @Override
    public void showNotficationInsideSnapspot(Snapspot _snapspot) {
        if (!App.get().isHomeFragmentInForeground()) {
            Intent i = new Intent(this, MainActivity.class);
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

            Notification.Builder builder = new Notification.Builder(this)
                    .setContentTitle("You are inside of a Snapspot!")
                    .setContentText("Snapspot: " + _snapspot.getName())
                    .setSmallIcon(R.drawable.ic_wandershot_location_marker)
                    .setContentIntent(pi);

            Notification notification = builder.build();

            NotificationManager notificationManager = (NotificationManager) getSystemService
                    (Context.NOTIFICATION_SERVICE);
            notificationManager.notify(LOCATION_SERVICE_NOTIFICATION, notification);
        } else {

            if(!mSkipNextDialog){
                mShowDialogToUser = true;
            } else{
                mSkipNextDialog=false;
            }
        }
    }

    /**
     * Gets called from the LocationHelper if the user currently is nearby of multiple Snapspots
     * Shows a Notification if the app runs in background
     *
     * @param _snapspotsNearby Count of Snapspots the user is nearby
     * @see Snapspot
     * @see eu.codingtub.wandershot.api.location.LocationHelper
     */
    @Override
    public void showNotficationMultipleSnapspotsNearby(int _snapspotsNearby) {
        if (!App.get().isHomeFragmentInForeground()) {
            Intent i = new Intent(this, MainActivity.class);
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

            Notification.Builder builder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_wandershot_location_marker)
                    .setContentTitle(getString(R.string.notification_title_multiple_nearby))
                    .setContentText(getString(R.string.notification_text_nearby))
                    .setContentIntent(pi);

            Notification notification = builder.build();

            NotificationManager notificationManager = (NotificationManager) this.getSystemService
                    (Context.NOTIFICATION_SERVICE);
            notificationManager.notify(LOCATION_SERVICE_NOTIFICATION, notification);
        }
    }

    /**
     * Gets called from the LocationHelper if the user currently is nearby of a Snapspots
     * Shows a Notification if the app runs in background
     *
     * @see Snapspot
     * @see eu.codingtub.wandershot.api.location.LocationHelper
     */
    @Override
    public void showNotficationSingleSnapspotNearby() {
        if (!App.get().isHomeFragmentInForeground()) {
            Intent i = new Intent(this, MainActivity.class);
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);

            Notification.Builder builder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_wandershot_location_marker)
                    .setContentTitle(getString(R.string.notification_title_single_nearby))
                    .setContentText(getString(R.string.notification_text_nearby))
                    .setContentIntent(pi);

            Notification notification = builder.build();

            NotificationManager notificationManager = (NotificationManager) this.getSystemService
                    (Context.NOTIFICATION_SERVICE);
            notificationManager.notify(LOCATION_SERVICE_NOTIFICATION, notification);
        }
    }
}
