package eu.codingtub.wandershot.utils;

import eu.codingtub.wandershot.R;

/**
 * A enum used to distinguish between different snapspot-categories
 *
 * @author Martin Schneglberger
 * @see eu.codingtub.wandershot.models.Snapspot
 */

public enum SnapspotCategory {
    LANDSCAPE(1, "landscape", R.mipmap.snap_needle_green, R.mipmap.snap_icon_green),
    NUTRIMENT(2, "nutriment", R.mipmap.snap_needle_blue, R.mipmap.snap_icon_blue),
    SIGHT(3, "sight", R.mipmap.snap_needle_yellow, R.mipmap.snap_icon_yellow),
    OTHER(4, "other", R.mipmap.snap_needle_red, R.mipmap.snap_icon_red);

    private final int mCode;
    private final String mType;
    private final int mResourceNeedle;
    private final int mResourceIcon;

    /**
     * Constructor
     *
     * @param _code           Integer holding the code for this category
     * @param _type           Type in form of a String
     * @param _resourceNeedle Resource-ID of the snapspot-needle for this category
     * @param _resourceIcon   Resource-ID of the snapspot-icon for this category
     */
    SnapspotCategory(int _code, String _type, int _resourceNeedle, int _resourceIcon) {
        mCode = _code;
        mType = _type;
        mResourceNeedle = _resourceNeedle;
        mResourceIcon = _resourceIcon;
    }

    /**
     * Gets the type of this category
     *
     * @return Name of category as a String
     */
    public String getType() {
        return mType;
    }

    /**
     * Gets the Resource ID for the snapspot-needle assigned to this category
     *
     * @return Resource-ID to the snapspot-drawable
     */
    public int getResourceNeedle() {
        return mResourceNeedle;
    }

    /**
     * Gets the Resource ID for the snapspot-icon assigned to this category
     *
     * @return Resource-ID to the snapspot-drawable
     */
    public int getResourceIcon() {
        return mResourceIcon;
    }
}
