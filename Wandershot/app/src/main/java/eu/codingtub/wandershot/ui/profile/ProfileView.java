package eu.codingtub.wandershot.ui.profile;

import com.hannesdorfmann.mosby.mvp.MvpView;

import eu.codingtub.wandershot.models.WandershotUser;

/**
 * The declaration of methods used in the ProfileFragment
 */

public interface ProfileView extends MvpView {

    /**
     * Sets all view labels and ImageViews according to the received user data.
     *
     * @param _user the currently active WandershotUser
     * @see eu.codingtub.wandershot.models.WandershotUser
     */
    void setUserData(WandershotUser _user);

    /**
     * Saves the data of a WandershotUser to the SharedPreferences.
     *
     * @param _user the currently active WandershotUser
     * @see eu.codingtub.wandershot.models.WandershotUser
     */
    void saveUserDataToSharedPreferences(WandershotUser _user);

    /**
     * Shows the user an error message
     */
    void showDataFetchError();

    /**
     * Starts the UserSnapspotsActivity to show the visited Snapspots.
     */
    void startUserSnapspotsActivity();
}
