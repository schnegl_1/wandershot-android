package eu.codingtub.wandershot.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;

import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.ui.profile.usersnapspots.UserSnapspotView;

/**
 * The presenter for the UserSnapSpotActivity.
 *
 * @author Michael Rieger
 */

public class UserSnapspotPresenter extends MvpBasePresenter<UserSnapspotView> implements OnSnapspotsReceivedListener {

    private AuthenticationHelper mAuthenticationHelper;
    private DatabaseHelper mDatabaseHelper;

    /**
     * Constructor
     *
     * @param _authenticationHelper the apps {@link AuthenticationHelper} instance
     * @param _databaseHelper       the apps {@link DatabaseHelper} instance
     */
    public UserSnapspotPresenter(AuthenticationHelper _authenticationHelper, DatabaseHelper _databaseHelper) {
        mAuthenticationHelper = _authenticationHelper;
        mDatabaseHelper = _databaseHelper;
    }

    /**
     * Fetch the users snapspots from the database
     */
    public void getSnapspotsFromDatabase() {
        mDatabaseHelper.getUserSnapspots(mAuthenticationHelper.getCurrentUser(), this);
    }

    /**
     * Callback of the {@link DatabaseHelper}.
     * Show all markers on the map.
     *
     * @param _snapspots the list of Snapspots
     * @see eu.codingtub.wandershot.models.Snapspot
     */
    @Override
    public void onSnapspotsReceived(ArrayList<Snapspot> _snapspots) {
        getView().setMarkers(_snapspots);
    }

    /**
     * Callback of the {@link DatabaseHelper}.
     * Show an error message on the view.
     */
    @Override
    public void onSnapspotsReceiveFailed() {
        getView().showSnapSpotReceiveFailed();
    }
}
