package eu.codingtub.wandershot.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.presenter.LoginPresenter;
import eu.codingtub.wandershot.ui.main.MainActivity;
import eu.codingtub.wandershot.ui.registration.RegisterActivity;

/**
 * The Login Activity class.
 *
 * @author Michael Rieger
 */

public class LoginActivity extends MvpActivity<LoginView, LoginPresenter> implements LoginView, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.login_email)
    EditText mEmailEditText;

    @BindView(R.id.login_password)
    EditText mPasswordEditText;

    ProgressDialog mProgressDialog;

    public int REQUEST_CODE_GOOGLE_SIGN_IN = 989;

    /**
     * Creates the presenter that is used for this Activity.
     *
     * @return a LoginPresenter instance
     */
    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter(App.get().getAuthenticationHelper(), App.get().getDatabaseHelper());
    }

    /**
     * The Activity's onCreate method.
     * <p>
     * Configures the GoogleSignInOptions and sets the GoogleApiClient for this Activity and starts
     * the MainActivity if the user is already signed in.
     * </p>
     *
     * @param _savedInstanceState the saved instance state Bundle
     * @see com.google.android.gms.common.api.GoogleApiClient
     * @see com.google.android.gms.auth.api.signin.GoogleSignInOptions
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        presenter.startMainActivityIfSignedIn();
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        presenter.setGoogleApiClient(
                new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build());
    }

    /**
     * Starts the MainActivity.
     */
    @Override
    public void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Starts the RegisterActivity.
     */
    @Override
    public void startRegisterActivity() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    /**
     * Starts the Intent for choosing a Google Account for a sign in via Google.
     *
     * @param _googleApiClient the GoogleApiClient instance
     * @see com.google.android.gms.common.api.GoogleApiClient
     */
    public void signInViaGoogle(GoogleApiClient _googleApiClient) {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(_googleApiClient);
        startActivityForResult(intent, REQUEST_CODE_GOOGLE_SIGN_IN);
    }

    /**
     * Shows the user that either the email or password field was left empty.
     */
    @Override
    public void showInputError() {
        Toast.makeText(this, "Please enter your credentials", Toast.LENGTH_SHORT).show();
    }

    /**
     * Starts a ProgressDialog showing the user that a Login is being performed.
     */
    @Override
    public void showProgressDialog() {
        final String dialogText = getString(R.string.login_sign_in_dialog);
        // TODO: 17/01/2017 set dialog style
        mProgressDialog = ProgressDialog.show(this, dialogText, dialogText);
    }

    /**
     * Stops a currently running ProgressDialog.
     */
    @Override
    public void stopProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Shows the user that the sign in process was successful and forward to the MainActivity.
     */
    @Override
    public void logInSuccessful() {
        Toast.makeText(this, "Login was successful", Toast.LENGTH_SHORT).show();

        presenter.startMainActivityIfSignedIn();
    }

    /**
     * Shows the user that the sign in process failed and displays an error message.
     *
     * @param _exception the message of the exception that was caught while signing in
     */
    @Override
    public void logInFailed(String _exception) {
        Toast.makeText(this, "Login failed. Please check your credentials - " + _exception,
                Toast.LENGTH_LONG).show();
    }

    /**
     * The OnClick method of the login button. Passes email address and password to the presenter.
     */
    @OnClick(R.id.login_button)
    public void logInClicked() {
        final String email = mEmailEditText.getText().toString();
        final String password = mPasswordEditText.getText().toString();
        presenter.signInWithEmailAndPassword(email, password);
    }

    /**
     * The OnClick method of the register button.
     */
    @OnClick(R.id.login_register_button)
    public void startRegistration() {
        presenter.startRegisterActivity();
    }

    /**
     * The OnClick method of the Google Sign In button.
     */
    @OnClick(R.id.login_google_button)
    public void signInWithGoogleClicked() {
        presenter.signInWithGoogle();
    }

    /**
     * Catches the Activity's result and the data that may be passed.
     *
     * @param requestCode the requestCode as int
     * @param resultCode  the resultCode as int
     * @param data        the data as Intent
     * @see android.content.Intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            presenter.handleSignInResult(result);
        }
    }

    /**
     * Is called when the connection to the Google sign in service failed.
     *
     * @param _connectionResult the connection result
     * @see com.google.android.gms.common.ConnectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult _connectionResult) {
        Toast.makeText(this, "Connection failed!", Toast.LENGTH_SHORT).show();

        Log.e("LoginActivity", "Connection failed: " + _connectionResult.getErrorMessage());
    }
}
