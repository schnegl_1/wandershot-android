package eu.codingtub.wandershot.ui.profile.usersnapspots;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.presenter.UserSnapspotPresenter;
import eu.codingtub.wandershot.service.LocationService;

/**
 * Shows a MapView with marker of all Snapspots that the user visited before.
 *
 * @author Michael Rieger
 */

public class UserSnapspotsActivity extends MvpActivity<UserSnapspotView, UserSnapspotPresenter> implements UserSnapspotView, OnMapReadyCallback {

    @BindView(R.id.usersnapspots_mapview)
    MapView mMapView;

    private GoogleMap mGoogleMap;

    private static final int REQUEST_ACCESS_FINE_LOCATION = 2;

    @NonNull
    @Override
    public UserSnapspotPresenter createPresenter() {
        return new UserSnapspotPresenter(App.get().getAuthenticationHelper(), App.get().getDatabaseHelper());
    }

    /**
     * Initialize ButterKnife binding, set the Title/Actionbar and load the map.
     *
     * @param _savedInstanceState the savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_usersnapspots);

        ButterKnife.bind(this);

        this.getSupportActionBar().setTitle(R.string.usersnapspots_title);

        mMapView.onCreate(_savedInstanceState);
        mMapView.getMapAsync(this);
    }

    /**
     * Adjust the mapview if it is ready. Also check permissions for fine location since the user's
     * position should also be shown.
     *
     * @param _googleMap the GoogleMap instance
     */
    @Override
    public void onMapReady(GoogleMap _googleMap) {
        mGoogleMap = _googleMap;

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        } else {
            adjustMapView();
        }
    }

    /**
     * Gets called after the GoogleMap for this Fragment is initialized.
     * Sets all UI-Elements and Properties which are desired for providing a map
     *
     * @see GoogleMap
     */
    private void adjustMapView() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
            mGoogleMap.getUiSettings().setCompassEnabled(true);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
            mGoogleMap.setBuildingsEnabled(true);
            presenter.getSnapspotsFromDatabase();
        }
    }

    /**
     * Callback which draws a Marker for each Snapspot read from the database onto the Map.
     * Additionally, a radius representing the dimension of the snapspot gets drawn.
     *
     * @param _snapspots ArrayList of Snapspots
     * @see LocationService
     */
    @Override
    public void setMarkers(ArrayList<Snapspot> _snapspots) {
        for (Snapspot spot : _snapspots) {
            mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(spot.getCategory().getResourceNeedle()))
                    .anchor(0.5f, 1.0f)
                    .position(spot.getLatLng())
                    .title(spot.getName()));
        }
    }

    @Override
    public void showSnapSpotReceiveFailed() {
        Toast.makeText(this, "Couldn't load your visited Snapspots.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Handles the permission request results.
     * <p>
     * If the permission ACCESS_FINE_LOCATION was granted, the user will be able to use his gps
     * sensor.
     *
     * @param _requestCode  the request code as int
     * @param _permissions  the permissions as String[]
     * @param _grantResults the granted permissions as int[]
     * @see java.lang.String
     */
    @Override
    public void onRequestPermissionsResult(int _requestCode, String _permissions[], int[] _grantResults) {
        switch (_requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (_grantResults.length > 0
                        && _grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    adjustMapView();
                } else {
                    Toast.makeText(this, "Damn it I need this permission!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

    /**
     * Activity's onResume()-method
     */
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    /**
     * Activity's onDestroy()-method
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    /**
     * Activity's onPause()-method
     */
    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    /**
     * Activity's onLowMemory()-method
     */
    @Override
    public void onLowMemory() {
        mMapView.onLowMemory();
        super.onLowMemory();
    }
}
