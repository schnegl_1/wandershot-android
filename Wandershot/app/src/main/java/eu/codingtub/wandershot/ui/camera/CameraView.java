package eu.codingtub.wandershot.ui.camera;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.io.File;

/**
 * Definitions of methods needed for the CameraActivity.
 *
 * @author Michael Rieger
 */

public interface CameraView extends MvpView {

    /**
     * Takes a picture.
     */
    void takePicture();

    /**
     * Switches the currently active camera (Front/Back).
     */
    void switchCamera();

    /**
     * Shows the given message to the user.
     *
     * @param _message the message that should be displayed
     * @see java.lang.String
     */
    void showMessage(String _message);

    /**
     * Crops the image given as parameter.
     *
     * @param _file the image that should be edited
     * @see java.io.File
     */
    void cropImage(File _file);
}
