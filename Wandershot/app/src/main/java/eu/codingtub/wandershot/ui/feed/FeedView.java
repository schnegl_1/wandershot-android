package eu.codingtub.wandershot.ui.feed;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Definitions of methods needed for the FeedFragment.
 * Should stay empty, because the RecyclerView is not designed to get abstracted into MVP-Pattern
 *
 * @author Martin Schneglberger
 */

public interface FeedView extends MvpView {
}
