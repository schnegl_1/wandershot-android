package eu.codingtub.wandershot.ui.registration;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.presenter.RegisterPresenter;
import eu.codingtub.wandershot.ui.main.MainActivity;
import eu.codingtub.wandershot.utils.Error;
import eu.codingtub.wandershot.utils.SharedPrefConstants;
import eu.codingtub.wandershot.utils.StringUtils;

/**
 * The Activity that is used for the registration of an user.
 */

public class RegisterActivity extends MvpActivity<RegisterView, RegisterPresenter> implements RegisterView {
    @BindView(R.id.register_name)
    EditText mNameEditText;

    @BindView(R.id.register_email)
    EditText mEmailEditText;

    @BindView(R.id.register_birthday)
    EditText mBirthdayEditText;

    @BindView(R.id.register_country)
    EditText mCountryEditText;

    @BindView(R.id.register_password)
    EditText mPasswordEditText;

    @BindView(R.id.pic_image)
    ImageView mProfilePictureView;

    private ProgressDialog mProgressDialog;

    private static int DATEPICKER_YEAR = 1996;
    private static int DATEPICKER_MONTH = 6;
    private static int DATEPICKER_DAY = 15;

    private static final int RESULT_LOAD_IMG = 1;
    private static final int REQUEST_READ_WRITE_STORAGE = 1;

    /**
     * Creates the presenter that is used for this Activity.
     *
     * @return a RegisterPresenter instance
     */
    @NonNull
    @Override
    public RegisterPresenter createPresenter() {
        return new RegisterPresenter(App.get().getDatabaseHelper(), App.get().getAuthenticationHelper());
    }

    /**
     * The Activity's onCreate method.
     *
     * @param _savedInstanceState the savedInstanceState Bundle
     * @see com.google.android.gms.common.api.GoogleApiClient
     * @see com.google.android.gms.auth.api.signin.GoogleSignInOptions
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    /**
     * The OnClick method of the register button.
     * <p>
     * Gets all attributes from the text fields and passes it to the presenter, which will try to
     * register and initialise the user.
     */
    @OnClick(R.id.register_register_button)
    public void registerUser() {
        final String name = mNameEditText.getText().toString();
        final String email = mEmailEditText.getText().toString();
        final String birthday = mBirthdayEditText.getText().toString();
        final String country = mCountryEditText.getText().toString();
        final String password = mPasswordEditText.getText().toString();

        presenter.registerUser(name, email, country, birthday, password);
    }

    /**
     * The OnClick method of the EditText field for the birthday. Calls the DatePicker.
     */
    @OnClick(R.id.register_birthday)
    public void getBirthday() {
        presenter.getBirthday();
    }

    /**
     * The OnFocusChange method of the EditText field for the birthday. Calls the DatePicker.
     *
     * @param _hasFocus the focus state as boolean
     */
    @OnFocusChange(R.id.register_birthday)
    public void getBirthday(boolean _hasFocus) {
        if (_hasFocus) {
            presenter.getBirthday();
        }
    }

    /**
     * The OnClick method of the EditText field for the country. Calls the CountryPicker.
     */
    @OnClick(R.id.register_country)
    public void getCountry() {
        presenter.getBirthday();
    }

    /**
     * The OnFocusChange method of the EditText field for the country. Calls the CountryPicker.
     *
     * @param _hasFocus the focus state as boolean
     */
    @OnFocusChange(R.id.register_country)
    public void getCountry(boolean _hasFocus) {
        if (_hasFocus) {
            presenter.getCountry();
        }
    }

    /**
     * The OnClick method for the picture selection. Requests permission and lets the user pick an
     * image from the camera roll.
     */
    @OnClick(R.id.pic_image)
    public void selectPicture() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_READ_WRITE_STORAGE);
        } else {
            presenter.selectPictureFromGallery();
        }
    }

    @OnClick(R.id.register_login_button)
    public void navigateToLogin() {
        presenter.navigateBack();
    }

    /**
     * Starts the MainActivity.
     */
    @Override
    public void startMainActivity() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    @Override
    public void resetSharedPreferences() {
        SharedPreferences.Editor editor = this.getPreferences(Context.MODE_PRIVATE).edit();

        // save user data in SharedPreferences
        editor.putString(SharedPrefConstants.KEY_USERID, null);
        editor.putString(SharedPrefConstants.KEY_NAME, null);
        editor.putString(SharedPrefConstants.KEY_EMAIL, null);
        editor.putString(SharedPrefConstants.KEY_COUNTRY, null);
        editor.putString(SharedPrefConstants.KEY_BIRTHDAY, null);
        editor.putString(SharedPrefConstants.KEY_PROFILE_PICTURE, null);

        editor.apply();
    }

    /**
     * Shows the user if an attribute was passed empty and has to be entered in the view's text.
     *
     * @param _error the error that occurred as Error
     * @see eu.codingtub.wandershot.utils.Error
     */
    @Override
    public void showAttributeError(Error _error) {
        switch (_error) {
            case NAME_EMPTY:
                mNameEditText.setError(Error.NAME_EMPTY.toString());
            case EMAIL_EMPTY:
                mEmailEditText.setError(Error.EMAIL_EMPTY.toString());
            case BIRTHDAY_EMPTY:
                mBirthdayEditText.setError(Error.BIRTHDAY_EMPTY.toString());
            case COUNTRY_EMPTY:
                mCountryEditText.setError(Error.COUNTRY_EMPTY.toString());
            case PASSWORD_EMPTY:
                mPasswordEditText.setError(Error.PASSWORD_EMPTY.toString());
            default:
                mNameEditText.setError(null);
                mEmailEditText.setError(null);
                mBirthdayEditText.setError(null);
                mCountryEditText.setError(null);
                mPasswordEditText.setError(null);
        }
    }

    /**
     * Shows the user which error occurred while registration was in progress.
     *
     * @param _exception the message of the exception that occurred as String
     * @see java.lang.String
     */
    @Override
    public void showRegistrationError(String _exception) {
        Toast.makeText(this, "There was an error while registration." + _exception,
                Toast.LENGTH_LONG).show();
    }

    /**
     * Shows a DatePicker to let the user select the birthday.
     */
    @Override
    public void getBirthdayFromDialog() {
        DatePickerDialog.OnDateSetListener dateDialogStateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //month+1 because of starting with zero
                month += 1;
                String date = StringUtils.checkDateLeadingZero(dayOfMonth) +
                        "." + StringUtils.checkDateLeadingZero(month) +
                        "." + year;
                mBirthdayEditText.setText(date);
            }
        };

        int year = DATEPICKER_YEAR;
        int month = DATEPICKER_MONTH;
        int day = DATEPICKER_DAY;

        DatePickerDialog dateDialog = new DatePickerDialog(this, R.style.AlertDialogTheme, dateDialogStateListener,
                year, month, day);

        dateDialog.show();
    }

    /**
     * Shows a CountryPicker to let the user choose the country.
     */
    @Override
    public void getCountryFromDialog() {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                mCountryEditText.setText(name);
                picker.dismiss();
            }
        });
    }

    /**
     * Shows the user a gallery to pick a profile picture from.
     */
    @Override
    public void selectImageFromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    /**
     * Shows a ProgressDialog to the user.
     */
    @Override
    public void showProgressDialog() {
        final String dialogText = getString(R.string.register_setup_account_dialog);
        // TODO: 17/01/2017 set dialog style
        mProgressDialog = ProgressDialog.show(this, dialogText, dialogText);
    }

    /**
     * Stops the previously started ProgressDialog.
     */
    @Override
    public void stopProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void navigateUp() {
        onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                presenter.navigateBack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void cropImage(Uri _sourceUri, Uri _destinationUri) {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_READ_WRITE_STORAGE);
        } else {
            startCropping(_sourceUri, _destinationUri);
        }
    }

    private void startCropping(Uri _sourceUri, Uri _destinationUri) {
        UCrop.Options cropOptions = new UCrop.Options();
        cropOptions.setShowCropGrid(true);
        cropOptions.setCircleDimmedLayer(true);
        cropOptions.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        cropOptions.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        cropOptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));

        UCrop.of(_sourceUri, _destinationUri)
                .withAspectRatio(1, 1)
                .withMaxResultSize(800, 800)
                .withOptions(cropOptions)
                .start(this);
    }

    private Uri getDestinationPath() {
        File dir = this.getDir("wandershot", Context.MODE_PRIVATE); //Creating an internal dir;
        File destPath = new File(dir, "profile-picture.jpg");
        return Uri.fromFile(destPath);
    }

    /**
     * Handles the permission request results.
     * <p>
     * If the permission READ_EXTERNAL_STORAGE was granted, the user will be able to select a picture
     * from the camera roll.
     *
     * @param requestCode  the request code as int
     * @param permissions  the permissions as String[]
     * @param grantResults the granted permissions as int[]
     * @see java.lang.String
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImageFromGallery();
                } else {
                    Toast.makeText(this, "Damn it I need this permission!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

    /**
     * Handles the Activity's results.
     * <p>
     * If a picture was chosen from camera roll it will be converted to a file path that can be handled
     * properly and the chosen picture will be displayed on an ImageView.
     *
     * @param _requestCode the request code as int
     * @param _resultCode  the result code as int
     * @param _data        the data as Intent
     * @see android.content.Intent
     */
    @Override
    protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
        super.onActivityResult(_requestCode, _resultCode, _data);

        try {
            // When an Image is picked
            if (_resultCode == RESULT_OK && _requestCode == RESULT_LOAD_IMG) {
                // Get the Image from data
                Uri selectedImage = _data.getData();

                // crop picture
                cropImage(selectedImage, getDestinationPath());
            } else if (_resultCode == RESULT_OK && _requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(_data);
                // load the selected picture to the ImageView
                Picasso.with(this)
                        .load(resultUri)
                        .transform(new CircleBorderTransform())
                        .placeholder(R.drawable.default_user)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(mProfilePictureView);

                presenter.setPicturePath(resultUri.getPath());
            } else if (_resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(_data);
            } else {
                Toast.makeText(this, "No picture selected.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Error handling picture.", Toast.LENGTH_SHORT).show();
        }
    }
}
