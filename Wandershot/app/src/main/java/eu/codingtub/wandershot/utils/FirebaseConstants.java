package eu.codingtub.wandershot.utils;

/**
 * A class containing final Strings with all existing DATABASE-names.
 */

public class FirebaseConstants {
    /**
     * Firebase storage reference URL
     */
    public static final String STORAGE_REFERENCE = "gs://wandershot-ab0ed.appspot.com";

    /**
     * Firebase storage profile picture folder
     */
    public static final String STORAGE_PROFILE_PICTURE = "profile_pictures";

    /**
     * Firebase storage snaps folder
     */
    public static final String STORAGE_SNAPS = "snaps";

    /**
     * Snapspot database
     */
    public static final String DATABASE_SNAPSPOTS = "snapspots";

    /**
     * User database
     */
    public static final String DATABASE_USERS = "user";

    /**
     * Metadata username identifier
     */
    public static final String META_USERNAME = "username";

    /**
     * Metadata snapspot name identifier
     */
    public static final String META_SNAPSPOTNAME = "snapspotname";

    /**
     * Metadata latitude identifier
     */
    public static final String META_LATITUDE = "latitude";

    /**
     * Metadata longitude identifier
     */
    public static final String META_LONGITUDE = "longitude";
}
