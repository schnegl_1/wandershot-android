package eu.codingtub.wandershot.ui.map;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.ArrayList;

import eu.codingtub.wandershot.models.Snapspot;

/**
 * Definitions of methods needed for the HomeView.
 *
 * @author Martin Schneglberger
 * @author Michael Rieger
 */

public interface HomeView extends MvpView {

    /**
     * Gets called after the GoogleMap for this Fragment is initialized.
     * Sets all UI-Elements and Properties which are desired for providing a map
     */
    void adjustMapView();

    /**
     * Callback which draws a Marker for each Snapspot read from the database onto the Map.
     * Additionally, a radius representing the dimension of the snapspot gets drawn
     * <p>
     * When all Markers are drawn, the GPS-Service gets started
     * </p>
     *
     * @param _snapspots ArrayList of Snapspots
     * @see eu.codingtub.wandershot.service.LocationService
     */
    void setMarkers(ArrayList<Snapspot> _snapspots);

    /**
     * Shows an error that the receiving of the snapspots failed.
     */
    void showSnapspotReceiveError();

    /**
     * Opens the CameraActivity for taking a snap.
     */
    void startCameraActivity();

    /**
     * Shows a ProgressDialog that a snap is currently uploaded.
     */
    void showProgressDialog();

    /**
     * Stops the currently active ProgressDialog.
     */
    void stopProgressDialog();

    /**
     * Show that the snap was uploaded successfully.
     */
    void showSnapUploaded();

    /**
     * Show that the upload failed.
     */
    void showUploadFailed();

    void showUploadSucceded();

    void changeToFeedFragment();
}
