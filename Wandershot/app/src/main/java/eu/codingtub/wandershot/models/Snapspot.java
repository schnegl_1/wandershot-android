package eu.codingtub.wandershot.models;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Map;

import eu.codingtub.wandershot.utils.SnapspotNotificationTriggerDistance;
import eu.codingtub.wandershot.utils.SnapspotCategory;

/**
 * This class represents a Snapspot, which is a circular area around a GPS-Position.
 *
 * @author Martin Schneglberger
 */

@IgnoreExtraProperties
public class Snapspot {

    private int mID;
    private String mName;
    private double mLatitude;
    private double mLongitude;
    private int mRadiusInM;
    private int mVisits;
    private boolean mNotificationNearbyTriggered;
    private SnapspotNotificationTriggerDistance mTriggerDistance;
    private SnapspotCategory mCategory;

    /**
     * Constructor
     */
    public Snapspot() {

    }

    /**
     * Constructor
     *
     * @param _ID              int holding the id of the Snapspot
     * @param _name            String representing the name of the Snapspot
     * @param _latitude        double holding the latitude-coordinate
     * @param _longitude       double holding the longitude-coordinate
     * @param _radiusInM       int holding the radius of the Snapspot in meters
     * @param _visits          int counting the visits in this Snapspot
     * @param _triggerDistance the TriggerDistance for this Snapspot
     * @param _category        the category of this Snapspot
     * @see SnapspotNotificationTriggerDistance
     * @see SnapspotCategory
     */
    public Snapspot(int _ID, String _name, double _latitude, double _longitude, int _radiusInM,
                    int _visits, SnapspotNotificationTriggerDistance _triggerDistance,
                    SnapspotCategory _category) {
        mID = _ID;
        mName = _name;
        mLatitude = _latitude;
        mLongitude = _longitude;
        mRadiusInM = _radiusInM;
        mVisits = _visits;
        mTriggerDistance = _triggerDistance;
        mCategory = _category;
        mNotificationNearbyTriggered = false;
    }

    //Getter and Setters
    public int getID() {
        return mID;
    }

    public void setID(int _id) {
        mID = _id;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double _latitude) {
        this.mLatitude = _latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double _longitude) {
        this.mLongitude = _longitude;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getRadiusInM() {
        return mRadiusInM;
    }

    public void setRadiusInM(int mRadiusInM) {
        this.mRadiusInM = mRadiusInM;
    }

    public int getVisits() {
        return mVisits;
    }

    public void setVisits(int _visits) {
        mVisits = _visits;
    }


    public boolean isNotificationNearbyTriggered() {
        return mNotificationNearbyTriggered;
    }

    public void setNotificationNearbyTriggered(boolean _notificationNearbyTriggered) {
        mNotificationNearbyTriggered = _notificationNearbyTriggered;
    }

    public SnapspotNotificationTriggerDistance getTriggerDistance() {
        return mTriggerDistance;
    }

    public void setTriggerDistance(SnapspotNotificationTriggerDistance _triggerDistance) {
        mTriggerDistance = _triggerDistance;
    }

    public SnapspotCategory getCategory() {
        return mCategory;
    }

    public void setCategory(SnapspotCategory _category) {
        mCategory = _category;
    }

    /**
     * Returns the Coordinate of this Snapspot as a LatLng-Instance
     *
     * @return LatLng-instance of the current location
     * @see LatLng
     */
    public LatLng getLatLng() {
        return new LatLng(mLatitude, mLongitude);
    }

    /**
     * Calculates the distance of a Coordinate to this Snapspot
     *
     * @param _currentLocation LatLng of the current position
     * @return distance to this Snapspot as a float
     */
    public float getDistanceToSnapspot(LatLng _currentLocation) {
        float result[] = new float[3];
        Location.distanceBetween(mLatitude, mLongitude, _currentLocation.latitude,
                _currentLocation.longitude, result);

        float resultInM = result[0];        //Distance between the points themselves

        resultInM -= mRadiusInM;              //Distance to the snapspot

        return resultInM;
    }

    /**
     * Calculates the distance of a Coordinate to the TriggerDistance of this Snapspot
     *
     * @param _currentLocation LatLng of the current position
     * @return distance to the TriggerDistance of this Snapspot
     * @see SnapspotNotificationTriggerDistance
     */
    public float getDistanceToTriggerPoint(LatLng _currentLocation) {
        float resultInM = getDistanceToSnapspot(_currentLocation);

        resultInM -= mTriggerDistance.getDistanceInM(); //Distance to Trigger Point
        return resultInM;
    }
}
