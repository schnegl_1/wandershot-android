package eu.codingtub.wandershot.api.authentication;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseUser;

import eu.codingtub.wandershot.api.listener.OnAuthenticationListener;
import eu.codingtub.wandershot.api.listener.OnGoogleSignInListener;
import eu.codingtub.wandershot.api.listener.OnRegistrationListener;

/**
 * Provides methods that will be used in the process of database authentication.
 *
 * @author Michael Rieger
 */

public interface AuthenticationHelper {
    /**
     * Try to login the user at the database. Call method of {@link OnAuthenticationListener} according to
     * success state.
     *
     * @param _email    the email of the user as String
     * @param _password the password of the user as String
     * @param _listener the callback listener
     * @see java.lang.String
     * @see OnAuthenticationListener
     */
    void userSignIn(String _email, String _password, OnAuthenticationListener _listener);

    /**
     * Try to login the user via Google Sign in. Call method of {@link OnAuthenticationListener} according
     * to success state.
     *
     * @param _listener the callback listener
     * @param _result   the result of the sign in process
     * @see com.google.android.gms.auth.api.signin.GoogleSignInResult
     * @see OnAuthenticationListener
     */
    void googleSignIn(GoogleSignInResult _result, OnGoogleSignInListener _listener);

    /**
     * Try to register the user. Call method of {@link OnAuthenticationListener} according
     * to success state.
     *
     * @param _email              the email of the user as String
     * @param _country            the user's country as String
     * @param _birthday           the user's birthday as String formatted as DD.MM.YYYY
     * @param _password           the password of the user as String
     * @param _profilePicturePath the path of the user's profile picture
     * @param _listener           the callback listener
     * @see java.lang.String
     * @see OnAuthenticationListener
     */
    void registerUser(final String _name, final String _email, final String _country, final String _birthday,
                      final String _password, final String _profilePicturePath,
                      final OnRegistrationListener _listener);

    /**
     * Sign out the user from the database.
     */
    void signOut();

    /**
     * Checks if the user is currently signed in to the database.
     *
     * @return true if signed in, false otherwise
     */
    boolean isUserSignedIn();

    /**
     * Returns the currently signed in user.
     *
     * @return the currently signed in user as {@link FirebaseUser}
     * @see FirebaseUser
     */
    FirebaseUser getCurrentUser();

    /**
     * Returns an instance of an GoogleApiClient.
     *
     * @return the instance of an GoogleApiClient
     * @see com.google.android.gms.common.api.GoogleApiClient
     */
    GoogleApiClient getGoogleApiClient();

    /**
     * Sets this class' instance of the GoogleApiClient.
     *
     * @param _googleApiClient the google api client
     * @see com.google.android.gms.common.api.GoogleApiClient
     */
    void setGoogleApiClient(GoogleApiClient _googleApiClient);
}
