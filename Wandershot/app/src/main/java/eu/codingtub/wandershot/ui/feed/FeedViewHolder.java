package eu.codingtub.wandershot.ui.feed;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.storage.StorageReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.codingtub.wandershot.R;

/**
 * The ViewHolder of the ReyclerView.
 * Binds all Views to the class and make them accessible from outside.
 * Forwards long-presses on the embedded ImageView through the
 * {@link eu.codingtub.wandershot.ui.feed.FeedAdapter.OnFeedItemLongClickListener}
 *
 * @author Martin Schneglberger
 * @see android.support.v7.widget.RecyclerView.ViewHolder
 * @see eu.codingtub.wandershot.ui.feed.FeedAdapter.OnFeedItemLongClickListener
 */

public class FeedViewHolder extends RecyclerView.ViewHolder {

    public static final String TAG = "FeedViewHolder";

    @BindView(R.id.feed_item_text)
    TextView mFeedItemText;
    @BindView(R.id.feed_item_date)
    TextView mFeedItemDate;
    @BindView(R.id.feed_item_image)
    ImageView mFeedItemImage;
    @BindView(R.id.feed_item_profile_pic)
    ImageView mFeedItemProfilePic;
    @BindView(R.id.feed_item_progress_bar)
    ProgressBar mFeedItemProgressBar;

    private StorageReference mStorageReference;

    /**
     * Constructor for the ViewHolder
     *
     * @param _itemView View which the ViewHolder contains
     */
    public FeedViewHolder(View _itemView) {
        super(_itemView);
        ButterKnife.bind(this, _itemView);
    }

    public StorageReference getStorageReference() {
        return mStorageReference;
    }

    public void setStorageReference(StorageReference _storageReference) {
        mStorageReference = _storageReference;
    }

    /**
     * Sets callback-Interface for Long-Clicks on the ImageView.
     *
     * @param _listener OnFeedItemLongClickListener which acts as a callback for long-clicks
     * @see eu.codingtub.wandershot.ui.feed.FeedAdapter.OnFeedItemLongClickListener
     */
    public void setUpLongClickListener(final FeedAdapter.OnFeedItemLongClickListener _listener) {
        mFeedItemImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View _view) {
                _listener.onItemLongClickListener(FeedViewHolder.this);
                return true;
            }
        });
    }
}
