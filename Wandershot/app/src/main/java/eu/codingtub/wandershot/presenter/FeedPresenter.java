package eu.codingtub.wandershot.presenter;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import eu.codingtub.wandershot.ui.feed.FeedView;

/**
 * The presenter used for the FeedFragment
 * Should stay empty, because the RecyclerView is not designed to get abstracted into MVP-Pattern
 *
 * @author Martin Schneglberger
 */

public class FeedPresenter extends MvpBasePresenter<FeedView> {
}
