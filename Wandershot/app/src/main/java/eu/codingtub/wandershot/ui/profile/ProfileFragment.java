package eu.codingtub.wandershot.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.models.WandershotUser;
import eu.codingtub.wandershot.presenter.ProfilePresenter;
import eu.codingtub.wandershot.ui.profile.usersnapspots.UserSnapspotsActivity;
import eu.codingtub.wandershot.ui.registration.CircleBorderTransform;
import eu.codingtub.wandershot.utils.SharedPrefConstants;
import eu.codingtub.wandershot.utils.StringUtils;

/**
 * The ProfileFragment showing the users data in a fancy layout.
 *
 * @author Michael Rieger
 */

public class ProfileFragment extends MvpFragment<ProfileView, ProfilePresenter> implements ProfileView {

    @BindView(R.id.profile_imageView)
    ImageView mImageView;

    @BindView(R.id.profile_toolbar_layout)
    CollapsingToolbarLayout mToolbarLayout;

    @BindView(R.id.profile_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.profile_textview_email)
    TextView mTextViewEmail;

    @BindView(R.id.profile_textview_birthday)
    TextView mTextViewBirthday;

    @BindView(R.id.profile_textview_country)
    TextView mTextViewCountry;

    @Override
    public ProfilePresenter createPresenter() {
        return new ProfilePresenter(App.get().getAuthenticationHelper(), App.get().getDatabaseHelper());
    }

    /**
     * Set ToolBar and title.
     *
     * @param inflater           the layout inflater
     * @param container          the container as ViewGroup
     * @param savedInstanceState the savedInstanceState bundle
     * @return a view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Name");
        mToolbarLayout.setTitle("Name");
        return view;
    }

    /**
     * Get all user values from the SharedPreferences.
     * If the user data wasn't already fetched, retrieve it from the database.
     *
     * @param view               the view
     * @param savedInstanceState the savedInstanceState Bundle
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        // check if values for the user data are already in the SharedPreferences
        String userId = preferences.getString(SharedPrefConstants.KEY_USERID, null);
        String name = preferences.getString(SharedPrefConstants.KEY_NAME, null);
        String email = preferences.getString(SharedPrefConstants.KEY_EMAIL, null);
        String country = preferences.getString(SharedPrefConstants.KEY_COUNTRY, null);
        String birthday = preferences.getString(SharedPrefConstants.KEY_BIRTHDAY, null);
        String profilePicture = preferences.getString(SharedPrefConstants.KEY_PROFILE_PICTURE, null);

        // if not, fetch the data from the database and set it to SharedPrefs

        if (StringUtils.isNullOrEmpty(userId, name, email, country, birthday, profilePicture)) {
            presenter.fetchUserData();
        } else {
            WandershotUser user = new WandershotUser(userId, name, email, country, birthday, profilePicture);
            setUserData(user);
        }
    }

    /**
     * Saves the data of a WandershotUser to the SharedPreferences.
     *
     * @param _user the currently active WandershotUser
     * @see eu.codingtub.wandershot.models.WandershotUser
     */
    @Override
    public void saveUserDataToSharedPreferences(WandershotUser _user) {
        SharedPreferences.Editor editor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();

        // save user data in SharedPreferences
        editor.putString(SharedPrefConstants.KEY_USERID, _user.getUserId());
        editor.putString(SharedPrefConstants.KEY_NAME, _user.getName());
        editor.putString(SharedPrefConstants.KEY_EMAIL, _user.getEmail());
        editor.putString(SharedPrefConstants.KEY_COUNTRY, _user.getCountry());
        editor.putString(SharedPrefConstants.KEY_BIRTHDAY, _user.getBirthday());
        editor.putString(SharedPrefConstants.KEY_PROFILE_PICTURE, _user.getProfilePicture());

        editor.apply();
    }

    /**
     * Shows the user an error message
     */
    @Override
    public void showDataFetchError() {
        Toast.makeText(getActivity(), R.string.profile_error_receiving_data, Toast.LENGTH_SHORT).show();
    }

    /**
     * Starts the UserSnapspotsActivity to show the visited Snapspots.
     */
    @Override
    public void startUserSnapspotsActivity() {
        Intent intent = new Intent(this.getActivity(), UserSnapspotsActivity.class);
        startActivity(intent);
    }

    /**
     * Sets all view labels and ImageViews according to the received user data.
     *
     * @param _user the currently active WandershotUser
     * @see eu.codingtub.wandershot.models.WandershotUser
     */
    @Override
    public void setUserData(WandershotUser _user) {
        mToolbarLayout.setTitle(_user.getName());
        mTextViewEmail.setText(_user.getEmail());
        mTextViewCountry.setText(_user.getCountry());
        mTextViewBirthday.setText(_user.getBirthday());

        // Load the profile picture from the Internet
        Picasso.with(getContext())
                .load(_user.getProfilePicture())
                .placeholder(R.drawable.default_user)
                .transform(new CircleBorderTransform())
                .into(mImageView);
    }

    /**
     * The onClick-method of the {@link android.support.design.widget.FloatingActionButton}
     */
    @OnClick(R.id.profile_fab)
    public void onSnapspotsVisitedClicked() {
        presenter.onSnapspotsVisitedClicked();
    }
}
