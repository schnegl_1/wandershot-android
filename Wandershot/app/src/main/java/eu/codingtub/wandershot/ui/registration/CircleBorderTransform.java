package eu.codingtub.wandershot.ui.registration;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import com.squareup.picasso.Transformation;

/**
 * A extension class for use with Picasso that generates a circle image.
 *
 * @author Michael Rieger
 * @see com.squareup.picasso.Transformation
 */

public class CircleBorderTransform implements Transformation {

    /**
     * Transform the source bitmap into a round bitmap.
     *
     * @param _source the source Bitmap that should be
     * @return a new round Bitmap generated from the source
     * @see android.graphics.Bitmap
     */
    @Override
    public Bitmap transform(Bitmap _source) {
        int size = Math.min(_source.getWidth(), _source.getHeight());

        int x = (_source.getWidth() - size) / 2;
        int y = (_source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(_source, x, y, size, size);
        if (squaredBitmap != _source) {
            _source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, _source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        canvas.drawARGB(0, 0, 0, 0);
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setColor(Color.WHITE);
        paint2.setStrokeWidth(16);
        canvas.drawCircle(r, r, r - 8, paint2);

        squaredBitmap.recycle();
        return bitmap;
    }

    /**
     * Returns a unique key for the transformation, used for caching purposes.
     *
     * @return the unique key as String
     * @see java.lang.String
     */
    @Override
    public String key() {
        return "circle";
    }
}
