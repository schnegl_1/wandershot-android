package eu.codingtub.wandershot.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;

import eu.codingtub.wandershot.api.listener.OnSnapUploadListener;
import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.api.location.LocationHelper;
import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.ui.map.HomeView;

/**
 * The presenter used for the HomeFragment
 * <p>
 * Implements the OnSnapspotsReceivedListener for handling results from the database.
 * </p>
 *
 * @author Martin Schneglberger
 * @author Michael Rieger
 * @see OnSnapspotsReceivedListener
 */

public class HomePresenter extends MvpBasePresenter<HomeView>
        implements OnSnapspotsReceivedListener, OnSnapUploadListener {

    private LocationHelper mLocationHelper;
    private DatabaseHelper mDatabaseHelper;
    private AuthenticationHelper mAuthHelper;

    /**
     * Constructor
     */
    public HomePresenter(LocationHelper _locationHelper,
                         DatabaseHelper _databaseHelper,
                         AuthenticationHelper _authHelper) {
        mLocationHelper = _locationHelper;
        mDatabaseHelper = _databaseHelper;
        mAuthHelper = _authHelper;
    }

    /**
     * Requests the LocationHelper-instance to load the Snapspots from the database
     *
     * @see LocationHelper
     */
    public void getSnapspotsFromDatabase() {
        mLocationHelper.getSnapspots(this);
    }

    /**
     * Callback when the Snapspots got loaded successfully
     * Invokes the HomeFragment to draw the Markers onto the map
     *
     * @param _snapspots ArrayList of loaded Snapspots
     * @see Snapspot
     */
    @Override
    public void onSnapspotsReceived(ArrayList<Snapspot> _snapspots) {
        getView().setMarkers(_snapspots);
    }

    /**
     * Callback when the Snapspots got loaded with errors
     */
    @Override
    public void onSnapspotsReceiveFailed() {
        getView().showSnapspotReceiveError();
    }

    /**
     * Handle the take photo button onClick event
     */
    public void openCameraButtonClicked() {
        getView().startCameraActivity();
    }

    public void showFeedButtonClicked() {
        getView().changeToFeedFragment();
    }

    public void uploadSnap(String _imagePath, LatLng _postition) {
        Snapspot snapspot = mLocationHelper.getCurrentSnapspot();
        mDatabaseHelper.uploadSnap(mAuthHelper.getCurrentUser(), _imagePath, snapspot, this);
        getView().showProgressDialog();
    }

    /**
     * Called when the snap was uploaded.
     * <p>
     * Stop a currently running ProgressDialog and show a success message.
     * </p>
     *
     * @param _snapspot
     * @see eu.codingtub.wandershot.models.Snapspot
     */
    @Override
    public void onSnapUploaded(Snapspot _snapspot) {
        getView().stopProgressDialog();
        getView().showUploadSucceded();
    }

    /**
     * Called when the upload failed.
     * <p>
     * Stop the currently running ProgressDialog and show an error message.
     * </p>
     *
     * @param _exception the error that occurred while performing the action as String
     */
    @Override
    public void OnSnapUploadFailed(String _exception) {
        getView().stopProgressDialog();
        getView().showUploadFailed();
    }
}
