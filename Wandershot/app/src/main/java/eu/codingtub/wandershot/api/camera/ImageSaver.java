package eu.codingtub.wandershot.api.camera;

import android.media.Image;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Saves a JPEG {@link Image} into the specified {@link File}.
 */
public class ImageSaver implements Runnable {

    /**
     * The JPEG image
     */
    private final Image mImage;

    /**
     * The file we save the image into.
     */
    private final File mFile;

    /**
     * The callback listener.
     */
    private OnImageSavedListener mListener;

    /**
     * Constructor
     *
     * @param _image    the Image that should be saved
     * @param _file     the File the image is saved into
     * @param _listener the callback listener
     * @see
     */
    public ImageSaver(Image _image, File _file, OnImageSavedListener _listener) {
        mImage = _image;
        mFile = _file;
        mListener = _listener;
    }

    /**
     * Writes an Image to a File
     *
     * @see android.media.Image
     */
    @Override
    public void run() {
        // Get bytes and write into a File
        ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        FileOutputStream output = null;

        try {
            output = new FileOutputStream(mFile);
            output.write(bytes);
            // Saved file
            mListener.onImageSaved(mFile);
        } catch (IOException e) {
            // Error: Call onFail method of listener
            mListener.onImageSaveFailed("Couldn't save file");
        } finally {
            // Close the output file stream
            mImage.close();
            if (null != output) {
                try {
                    output.close();
                } catch (IOException e) {
                    // Error: Call onFail method of listener
                    mListener.onImageSaveFailed("Couldn't save file");
                }
            }
        }
    }
}
