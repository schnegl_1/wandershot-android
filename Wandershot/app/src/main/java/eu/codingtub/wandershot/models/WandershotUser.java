package eu.codingtub.wandershot.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Representation of a Wandershot user.
 *
 * @author Martin Schneglberger
 */
@IgnoreExtraProperties
public final class WandershotUser {

    private String mUserId;
    private String mName;
    private String mEmail;
    private String mCountry;
    private String mBirthday;
    private String mProfilePicture;

    /**
     * Empty default constructor
     */
    public WandershotUser() {
    }

    /**
     * Constructor
     *
     * @param _userId   the users id as String
     * @param _name     the users name as String
     * @param _email    the users email address as String
     * @param _country  the users country as String
     * @param _birthday the users birthday formatted as DD.MM.YYYY as String
     * @see java.lang.String
     */
    public WandershotUser(String _userId, String _name, String _email, String _country, String _birthday) {
        this(_userId, _name, _email, _country, _birthday, null);
    }

    /**
     * Constructor
     *
     * @param _userId         the users id as String
     * @param _name           the users name as String
     * @param _email          the users email address as String
     * @param _country        the users country as String
     * @param _birthday       the users birthday formatted as DD.MM.YYYY as String
     * @param _profilePicture the users profile picture as String
     * @see java.lang.String
     */
    public WandershotUser(String _userId, String _name, String _email, String _country, String _birthday, String _profilePicture) {
        mUserId = _userId;
        mName = _name;
        mEmail = _email;
        mCountry = _country;
        mBirthday = _birthday;
        mProfilePicture = _profilePicture;
    }

    // Getter & Setter
    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String mBirthday) {
        this.mBirthday = mBirthday;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getProfilePicture() {
        return mProfilePicture;
    }

    public void setProfilePicture(String _profilePicture) {
        mProfilePicture = _profilePicture;
    }
}
