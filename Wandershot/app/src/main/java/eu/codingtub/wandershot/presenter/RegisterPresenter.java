package eu.codingtub.wandershot.presenter;

import com.google.firebase.auth.FirebaseUser;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import eu.codingtub.wandershot.api.listener.OnRegistrationListener;
import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.ui.registration.RegisterView;
import eu.codingtub.wandershot.utils.Error;
import eu.codingtub.wandershot.utils.StringUtils;

/**
 * The presenter used for the RegisterActivity.
 * <p>
 * Implements an OnRegistrationListener for handling registration results.
 * </p>
 *
 * @author Michael Rieger
 */

public class RegisterPresenter extends MvpBasePresenter<RegisterView> implements OnRegistrationListener {
    private DatabaseHelper mDatabaseHelper;
    private AuthenticationHelper mAuthHelper;

    private String mPicturePath;

    /**
     * Constructor
     *
     * @param _databaseHelper the applications DatabaseHelper instance
     * @param _authHelper     the applications AuthenticationHelper instance
     * @see eu.codingtub.wandershot.api.database.DatabaseHelper
     * @see eu.codingtub.wandershot.api.authentication.AuthenticationHelper
     */
    public RegisterPresenter(DatabaseHelper _databaseHelper, AuthenticationHelper _authHelper) {
        mDatabaseHelper = _databaseHelper;
        mAuthHelper = _authHelper;
    }

    /**
     * Controls all parameters for validity and registers the user at the database.
     * Starts a ProgressDialog on the view to signalise that an action is being performed.
     *
     * @param _name     the user's name as String
     * @param _email    the user's email address as String
     * @param _country  the user's country as String
     * @param _birthday the user's birthday formatted as DD.MM.YYYY as String
     * @param _password the user's password as String
     * @see java.lang.String
     */
    public void registerUser(String _name, String _email, String _country, String _birthday,
                             String _password) {

        if (StringUtils.isNullOrEmpty(_name)) {
            getView().showAttributeError(Error.NAME_EMPTY);
            //getView().showNameError();
        } else if (StringUtils.isNullOrEmpty(_email)) {
            getView().showAttributeError(Error.EMAIL_EMPTY);
            //getView().showEmailError();
        } else if (StringUtils.isNullOrEmpty(_password)) {
            getView().showAttributeError(Error.PASSWORD_EMPTY);
            //getView().showPasswordError();
        } else if (StringUtils.isNullOrEmpty(_country)) {
            getView().showAttributeError(Error.COUNTRY_EMPTY);
        } else if (StringUtils.isNullOrEmpty(_birthday)) {
            getView().showAttributeError(Error.BIRTHDAY_EMPTY);
        } else {
            mAuthHelper.registerUser(_name, _email, _country, _birthday, _password, mPicturePath, this);
            getView().showProgressDialog();
        }
    }

    /**
     * Shows a DatePicker on the view.
     */
    public void getBirthday() {
        getView().getBirthdayFromDialog();
    }

    /**
     * Shows the countryPicker on the view.
     */
    public void getCountry() {
        getView().getCountryFromDialog();
    }

    /**
     * Calls the view's method to select a profile picture from the gallery
     */
    public void selectPictureFromGallery() {
        getView().selectImageFromGallery();
    }

    /**
     * The callback method of the OnRegistrationListener that is called when the registration was
     * successful. This method calls the DatabaseHelper to initialise the user's preferences, stops
     * the ProgressDialog running on the view and calls the view's startMainActivity method afterwards.
     *
     * @param _name               the user's name as String
     * @param _email              the user's email address as String
     * @param _country            the user's country as String
     * @param _birthday           the user's birthday as String
     * @param _password           the password as String
     * @param _profilePicturePath the path of the user's profile picture as String
     * @see java.lang.String
     * @see OnRegistrationListener
     */
    @Override
    public void onUserRegistrated(String _name, String _email, String _country, String _birthday, String _password, String _profilePicturePath) {
        FirebaseUser currentUser = mAuthHelper.getCurrentUser();

        mDatabaseHelper.initUser(currentUser, _name, _email, _country, _birthday,
                _profilePicturePath, false);

        getView().stopProgressDialog();
        getView().resetSharedPreferences();
        getView().startMainActivity();
    }

    /**
     * The callback method of the OnRegistrationListener that is called when the registration failed.
     * Stops the ProgressDialog running on the view and calls the view's method to show the error
     * that occurred while registering the user.
     *
     * @param _exception the error that occurred while performing the action
     * @see java.lang.String
     * @see OnRegistrationListener
     */
    @Override
    public void onUserRegistrationFailed(String _exception) {
        getView().stopProgressDialog();
        getView().showRegistrationError(_exception);
    }

    /**
     * Set the profile picture path in this presenter instance.
     *
     * @param _picturePath the path to the user's profile picture as String
     * @see java.lang.String
     */
    public void setPicturePath(String _picturePath) {
        mPicturePath = _picturePath;
    }

    /**
     * Navigates back to the LoginActivity
     */
    public void navigateBack() {
        getView().navigateUp();
    }
}
