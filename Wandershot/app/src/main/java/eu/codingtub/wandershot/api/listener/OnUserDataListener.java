package eu.codingtub.wandershot.api.listener;

import eu.codingtub.wandershot.models.WandershotUser;

/**
 * This class acts as a callback listener used with database interaction when fetching user data.
 *
 * @author Michael Rieger
 */
public interface OnUserDataListener {

    /**
     * Is called when an action was successful
     *
     * @see eu.codingtub.wandershot.models.WandershotUser
     */
    void onUserDataReceived(WandershotUser _user);

    /**
     * Is called when an action failed
     *
     * @param _exception the error that occurred while performing the action as String
     * @see java.lang.String
     */
    void onUserDataReceiveFailed(String _exception);
}
