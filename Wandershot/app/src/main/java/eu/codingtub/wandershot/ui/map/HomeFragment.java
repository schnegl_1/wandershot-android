package eu.codingtub.wandershot.ui.map;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.dialogs.InsideSnapspotDialog;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.presenter.HomePresenter;
import eu.codingtub.wandershot.service.LocationService;
import eu.codingtub.wandershot.ui.camera.CameraActivity;
import eu.codingtub.wandershot.ui.feed.FeedFragment;

import static android.app.Activity.RESULT_OK;

/**
 * This Fragment represents the "Home"-Screen of the Application.
 * <p>
 * It shows a GoogleMap with all the snapspots in it, including their radius
 * When opening this Fragment, a GPS-Service is started which tracks the user's movement
 * </p>
 *
 * @author Martin Schneglberger
 * @author Michael Rieger
 * @see GoogleMap
 * @see Snapspot
 * @see LocationService
 */

public class HomeFragment extends MvpFragment<HomeView, HomePresenter> implements
        OnMapReadyCallback, HomeView, Handler.Callback {

    @BindView(R.id.map_open_camera_button)
    Button mOpenCameraButton;

    @BindView(R.id.map_toolbar)
    Toolbar mToolbar;

    private static final String TAG = "HomeFragment";
    private static final int TAKE_PHOTO_REQUEST = 3;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 2;
    public static final int REQUEST_LOCATION_SETTINGS = 999;
    public static final String MESSENGER_KEY = "LocationMessenger";
    public static final int SNAPSPOT_KEY = 1024;
    public static final int LOCATION_KEY = 2048;
    public static final int BUTTON_STATE_KEY = 4096;
    public static final int UPLOADED_SNAP_KEY = 8192;

    public static final String LOCATION_DATA_KEY = "Location";
    public static final String SNAPSPOT_DATA_KEY = "Snapspot";
    public static final String BUTTON_STATE_DATA_KEY = "ButtonState";
    public static final String UPLOADED_SNAP_DATA_KEY = "UploadedSnap";

    private MapView mMapView = null;

    private GoogleMap mGoogleMap;
    private Location mLastLocation;

    private ProgressDialog mProgressDialog;

    private Handler mHandler = null;
    private boolean mDialogShown;
    private boolean mUserUploadedPicture;

    private Activity mActivity;

    /**
     * Constructor
     */
    public HomeFragment() {
        // Required empty public constructor
        super();
    }

    /**
     * Creates the presenter that is used for this  Fragment.
     *
     * @return a HomePresenter instance
     */
    @NonNull
    @Override
    public HomePresenter createPresenter() {
        return new HomePresenter(App.get().getLocationHelper(),
                App.get().getDatabaseHelper(),
                App.get().getAuthenticationHelper());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    /**
     * The fragments onCreateView-method
     *
     * @param _inflater           LayoutInflater object that can be used to inflate any views in the fragment
     * @param _container          In non-null, this is the parent view the UI should be attached to
     * @param _savedInstanceState If non-null, this fragment is being re-constructed from a
     *                            previous saved state
     * @return View-hierarchy associated with the fragment
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container,
                             Bundle _savedInstanceState) {

        View v = _inflater.inflate(R.layout.fragment_map, _container, false);

        if (mMapView == null) {
            mMapView = (MapView) v.findViewById(R.id.map_mapView);
            mMapView.onCreate(_savedInstanceState);
            mMapView.getMapAsync(this);
        }

        if (mHandler == null) {
            mHandler = new Handler(this);
        }

        ButterKnife.bind(this, v);

        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Snapspots");

        return v;
    }

    /**
     * Called immediately after onCreateView
     *
     * @param view               The view returned from onCreateView
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a
     *                           previous saved state
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Callback when the GoogleMap requested in the onCreateView-method is ready.
     *
     * @param _googleMap Instance of the GoogleMap
     * @see GoogleMap
     */
    @Override
    public void onMapReady(GoogleMap _googleMap) {
        this.mGoogleMap = _googleMap;

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        } else {
            adjustMapView();
        }
    }

    /**
     * Gets called after the GoogleMap for this Fragment is initialized.
     * Sets all UI-Elements and Properties which are desired for providing a map
     *
     * @see GoogleMap
     */
    @Override
    public void adjustMapView() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
            mGoogleMap.getUiSettings().setCompassEnabled(true);
            mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
            mGoogleMap.setBuildingsEnabled(true);
            presenter.getSnapspotsFromDatabase();
        }
    }

    /**
     * Callback which draws a Marker for each Snapspot read from the database onto the Map.
     * Additionally, a radius representing the dimension of the snapspot gets drawn
     * <p>
     * When all Markers are drawn, the GPS-Service gets started
     *
     * @param _snapspots ArrayList of Snapspots
     * @see LocationService
     */
    @Override
    public void setMarkers(ArrayList<Snapspot> _snapspots) {
        if (mGoogleMap == null) {
            return;
        }

        for (Snapspot spot : _snapspots) {
            mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(spot.getCategory().getResourceNeedle()))
                    .anchor(0.5f, 1.0f)
                    .position(spot.getLatLng())
                    //.snippet("Visits: " + spot.getVisits())
                    .title(spot.getName()));

            CircleOptions circleOptions = new CircleOptions().center(spot.getLatLng())
                    .radius(spot.getRadiusInM())
                    .strokeColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                    .strokeWidth(1.0f)
                    .fillColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryLightTransparent));

            mGoogleMap.addCircle(circleOptions);
        }
        Intent serviceIntent = new Intent(getActivity(), LocationService.class);
        Messenger msgr = new Messenger(mHandler);
        serviceIntent.putExtra(MESSENGER_KEY, msgr);
        getActivity().startService(serviceIntent);

    }

    /**
     * Shows an error that the receiving of the snapspots failed.
     */
    @Override
    public void showSnapspotReceiveError() {
        Toast.makeText(getActivity(), "There was an error receiving data.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Opens the CameraActivity for taking a snap.
     */
    @Override
    public void startCameraActivity() {
        Intent intent = new Intent(getActivity(), CameraActivity.class);
        startActivityForResult(intent, TAKE_PHOTO_REQUEST);
    }

    /**
     * Handles the permission request results.
     * <p>
     * If the permission ACCESS_FINE_LOCATION was granted, the user will be able to use his gps
     * sensor.
     *
     * @param _requestCode  the request code as int
     * @param _permissions  the permissions as String[]
     * @param _grantResults the granted permissions as int[]
     * @see java.lang.String
     */
    @Override
    public void onRequestPermissionsResult(int _requestCode, String _permissions[], int[] _grantResults) {
        switch (_requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (_grantResults.length > 0
                        && _grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    adjustMapView();
                } else {
                    Toast.makeText(getActivity(), "Damn it I need this permission!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

    /**
     * Handles the messages received the messenger passed to the GPS-Service
     *
     * @param _message Message received from the GPS-Service
     * @return boolean signaling if the message was handled successfully
     * @see Handler
     * @see Messenger
     * @see LocationService
     */
    @Override
    public boolean handleMessage(Message _message) {
        if (_message != null) {
            switch (_message.what) {
                case SNAPSPOT_KEY: {
                    Bundle bundle = _message.getData();
                    String snapspotName = bundle.getString(SNAPSPOT_DATA_KEY, "undefined");
                    showDialog(snapspotName);
                    return true;
                }
                case LOCATION_KEY: {
                    mLastLocation = (Location) _message.obj;
                    double latitude = mLastLocation.getLatitude();
                    double longitude = mLastLocation.getLongitude();
                    LatLng position = new LatLng(latitude, longitude);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(position).zoom(15).build();
                    mGoogleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                    return true;
                }
                case BUTTON_STATE_KEY: {
                    Bundle bundle = _message.getData();
                    boolean hideBtn = bundle.getBoolean(BUTTON_STATE_DATA_KEY);
                    changeButtonState(hideBtn);
                    return true;
                }
                case UPLOADED_SNAP_KEY: {
                    Bundle bundle = _message.getData();
                    mUserUploadedPicture = bundle.getBoolean(UPLOADED_SNAP_DATA_KEY);
                    mOpenCameraButton.setText(R.string.map_open_feed);
                }
                default:
                    Log.d(TAG, "unknown message tag encountered");
            }
        }
        return false;
    }

    /**
     * Shows or hides the map-button
     *
     * @param _hideBtn boolean, true if button should not be visible
     */
    private void changeButtonState(boolean _hideBtn) {
        if (_hideBtn) {
            mOpenCameraButton.setVisibility(View.GONE);
        } else {
            mOpenCameraButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Handles the Activity's results.
     * <p>
     * If a picture was taken it will be converted to a file path that can be handled
     * properly and the chosen picture will be uploaded.
     * </p>
     *
     * @param _requestCode the request code as int
     * @param _resultCode  the result code as int
     * @param _data        the data as Intent
     * @see android.content.Intent
     */
    @Override
    public void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
        super.onActivityResult(_requestCode, _resultCode, _data);

        if (_resultCode == RESULT_OK && _requestCode == TAKE_PHOTO_REQUEST) {
            if (mLastLocation != null) {
                final String imagePath = _data.getStringExtra("ImagePath");
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                LatLng position = new LatLng(latitude, longitude);

                presenter.uploadSnap(imagePath, position);
            } else {
                showUploadFailed();
            }
        }
    }

    /**
     * Shows a Dialog if the user is inside of a Snapspot
     *
     * @param _snapspotName Name of the Snapspot the user is in
     * @see InsideSnapspotDialog
     */
    private void showDialog(String _snapspotName) {
        if (!mDialogShown) {
            mDialogShown = true;
            DialogFragment insideSnapspotDialog = InsideSnapspotDialog.newInstance(_snapspotName,
                    mUserUploadedPicture);
            insideSnapspotDialog.show(getActivity().getSupportFragmentManager(), "inside Snapspot");
        }
    }

    /**
     * Shows a ProgressDialog that a snap is currently uploaded.
     */
    @Override
    public void showProgressDialog() {
        final String dialogText = getString(R.string.map_uploading_snap);
        // TODO: 17/01/2017 set dialog style
        mProgressDialog = ProgressDialog.show(getActivity(), dialogText, dialogText);
    }

    /**
     * Stops the currently active ProgressDialog.
     */
    @Override
    public void stopProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Show that the snap was uploaded successfully.
     */
    @Override
    public void showSnapUploaded() {

    }

    /**
     * Show that the upload failed.
     */
    @Override
    public void showUploadFailed() {
        Toast.makeText(getActivity(), R.string.map_snap_upload_failed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUploadSucceded() {
        mOpenCameraButton.setText(R.string.map_open_feed);
        mUserUploadedPicture = true;
    }

    /**
     * Opens the FeedFragment which will show all the snaps which were uploaded inside of the
     * current Snapspot
     */
    @Override
    public void changeToFeedFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FeedFragment feedFragment = new FeedFragment();

        Bundle args = new Bundle();
        args.putString(FeedFragment.FEED_TYPE_KEY, FeedFragment.FEED_TYPE_SNAPSPOT);
        feedFragment.setArguments(args);

        fragmentTransaction.replace(R.id.main_fragment_container, feedFragment);
        fragmentTransaction.commit();
    }

    /**
     * OnClick method of the button that appears when entering the range of a Snapspot
     */
    @OnClick(R.id.map_open_camera_button)
    public void takePhotoClicked() {
        if (mUserUploadedPicture) {
            presenter.showFeedButtonClicked();
        } else {
            presenter.openCameraButtonClicked();
        }
    }

    /**
     * Fragments onStart()-method
     */
    @Override
    public void onStart() {
        App.get().setHomeFragmentInForeground(true);
        super.onStart();
    }

    /**
     * Fragments onStop()-method
     */
    @Override
    public void onStop() {
        App.get().setHomeFragmentInForeground(false);
        super.onStop();
    }

    /**
     * Fragments onResume()-method
     */
    @Override
    public void onResume() {
        mMapView.onResume();
        App.get().setHomeFragmentInForeground(true);
        super.onResume();
    }

    /**
     * Fragments onPause()-method
     */
    @Override
    public void onPause() {
        mMapView.onPause();
        App.get().setHomeFragmentInForeground(false);
        mDialogShown = false;
        //save state here (mLocationRequests....)
        super.onPause();
    }

    /**
     * Fragments onDestroy()-method
     */
    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        App.get().setHomeFragmentInForeground(false);
        super.onDestroy();
    }

    /**
     * Fragments onLowMemory()-method
     */
    @Override
    public void onLowMemory() {
        mMapView.onLowMemory();
        super.onLowMemory();
    }
}