package eu.codingtub.wandershot.ui.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.MvpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.presenter.MainPresenter;
import eu.codingtub.wandershot.ui.feed.FeedFragment;
import eu.codingtub.wandershot.ui.login.LoginActivity;
import eu.codingtub.wandershot.ui.map.HomeFragment;
import eu.codingtub.wandershot.ui.profile.ProfileFragment;
import eu.codingtub.wandershot.utils.SharedPrefConstants;

/**
 * The MainActivity class.
 *
 * @author Michael Rieger
 */

public class MainActivity extends MvpActivity<MainView, MainPresenter> implements MainView, BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MAIN_ACTIVITY";
    public static final String HOME_TAG = "HOME_FRAGMENT";

    private Fragment mCurrentFragment;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView mBottomNavigation;

    /**
     * Creates the presenter that is used for this Activity.
     *
     * @return a MainPresenter instance
     */
    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter(App.get().getAuthenticationHelper());
    }

    /**
     * The Activity's onCreate method.
     * <p>
     * Starts the login Activity if the user is not signed in and checks if GPS in enabled.
     * Selects the Home-Fragment (Snapspot-Map) automatically
     * </p>
     *
     * @param _savedInstanceState the saved instance state Bundle
     */
    @Override
    protected void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mBottomNavigation.setOnNavigationItemSelectedListener(this);
        presenter.startLoginActivityIfNotLoggedIn();

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGPS();
        }

        findViewById(R.id.menu_snapspots).performClick();
    }

    /**
     * Gets called to initialize the contents of the Activity's standard options menu.
     * <p>
     * The menu of the application is set in this method.
     * </p>
     *
     * @param _menu Menu which will be used for the activity
     * @return true Return true when the menu got inflated
     * @see Menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu _menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, _menu);
        return true;
    }

    /**
     * Handles item selection in the activity's actionbar menu
     *
     * @param _item MenuItem which caused the method-call
     * @return true if menu item got handled successfully
     * @see MenuItem
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem _item) {
        switch (_item.getItemId()) {
            case R.id.actionbar_logout:
                presenter.signOut();
                return true;
            default:
                return super.onOptionsItemSelected(_item);
        }
    }

    /**
     * Starts the LoginActivity and deletes the activity stack so signing in cannot be bypassed.
     */
    @Override
    public void startLoginActivity() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Resets the apps SharedPreferences.
     *
     * @see android.content.SharedPreferences.Editor
     */
    @Override
    public void resetSharedPreferences() {
        SharedPreferences.Editor editor = this.getPreferences(Context.MODE_PRIVATE).edit();

        // save user data in SharedPreferences
        editor.putString(SharedPrefConstants.KEY_USERID, null);
        editor.putString(SharedPrefConstants.KEY_NAME, null);
        editor.putString(SharedPrefConstants.KEY_EMAIL, null);
        editor.putString(SharedPrefConstants.KEY_COUNTRY, null);
        editor.putString(SharedPrefConstants.KEY_BIRTHDAY, null);
        editor.putString(SharedPrefConstants.KEY_PROFILE_PICTURE, null);

        editor.apply();
    }

    /**
     * Handle the items displayed in the bottom navigation and set the according fragment.
     *
     * @param _item the item that was chosen by the user
     * @return the switch state as boolean
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem _item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (_item.getItemId()) {
            case R.id.menu_feed: {
                if (!(mCurrentFragment instanceof FeedFragment)) {
                    FeedFragment feedFragment = new FeedFragment();

                    Bundle args = new Bundle();
                    args.putString(FeedFragment.FEED_TYPE_KEY, FeedFragment.FEED_TYPE_USER);
                    feedFragment.setArguments(args);

                    fragmentTransaction.replace(R.id.main_fragment_container, feedFragment);
                    mCurrentFragment = feedFragment;
                }
            }
            break;
            //case R.id.menu_search_user:{}break;
            case R.id.menu_snapspots: {
                if (!(mCurrentFragment instanceof HomeFragment)) {
                    HomeFragment homeFragment = new HomeFragment();
                    fragmentTransaction.replace(R.id.main_fragment_container, homeFragment,
                            HOME_TAG);
                    mCurrentFragment = homeFragment;
                }
            }
            break;
            case R.id.menu_profile:
                if (!(mCurrentFragment instanceof ProfileFragment)) {
                    ProfileFragment profileFragment = new ProfileFragment();
                    fragmentTransaction.replace(R.id.main_fragment_container, profileFragment);
                    mCurrentFragment = profileFragment;
                }
                break;
            default:
                Log.e(TAG, "Unexpected navigation-item selected");
                return false;
        }

        fragmentTransaction.commit();
        return true;
    }

    /**
     * Is used to check if GPS is enabled.
     *
     * @param requestCode the request code
     * @param resultCode  the result code
     * @param data        the data as Intent
     * @see android.content.Intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == HomeFragment.REQUEST_LOCATION_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    break;
                default:
                    Toast.makeText(this, "Please enable GPS!", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Shows an AlertDialog in case GPS is disabled. If user doesn't want to enable GPS, app
     * suspends
     */
    private void buildAlertMessageNoGPS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(eu.codingtub.wandershot.utils.Error.NO_GPS.getDescription())
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface _dialogInterface, int _i) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface _dialogInterface, int _i) {
                        _dialogInterface.cancel();
                        finish();
                    }
                });
        AlertDialog alertNoGPS = builder.create();
        alertNoGPS.show();
    }
}
