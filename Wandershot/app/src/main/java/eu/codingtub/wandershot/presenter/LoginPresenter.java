package eu.codingtub.wandershot.presenter;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import eu.codingtub.wandershot.api.listener.OnAuthenticationListener;
import eu.codingtub.wandershot.api.listener.OnGoogleSignInListener;
import eu.codingtub.wandershot.api.authentication.AuthenticationHelper;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.ui.login.LoginView;

/**
 * The presenter used for the LoginActivity.
 * <p>
 * Implements an OnAuthenticationListener for handling sign in results.
 * </p>
 *
 * @author Michael Rieger
 * @see OnAuthenticationListener
 */

public class LoginPresenter extends MvpBasePresenter<LoginView>
        implements OnAuthenticationListener, OnGoogleSignInListener {

    private AuthenticationHelper mAuthHelper;
    private DatabaseHelper mDatabaseHelper;

    /**
     * Constructor for the LoginPresenter.
     *
     * @param _authenticationHelper the application's AuthenticationHelper instance
     * @see eu.codingtub.wandershot.api.authentication.AuthenticationHelper
     */
    public LoginPresenter(AuthenticationHelper _authenticationHelper, DatabaseHelper _databaseHelper) {
        mAuthHelper = _authenticationHelper;
        mDatabaseHelper = _databaseHelper;
    }

    /**
     * Calls the view's startMainActivity method if the user is already signed in.
     */
    public void startMainActivityIfSignedIn() {
        if (mAuthHelper.isUserSignedIn()) {
            getView().startMainActivity();
        }
    }

    /**
     * Calls the view's startRegisterActivity method.
     */
    public void startRegisterActivity() {
        getView().startRegisterActivity();
    }

    /**
     * Tries to sign in the user via email and password. Calls the view's showInputError method if
     * one of the parameters is empty. If both parameters are passed correctly a ProgressDialog is
     * shown on the view via the view's showProgressDialog method.
     *
     * @param _email    the user#s email address as String
     * @param _password the user's password as String
     */
    public void signInWithEmailAndPassword(String _email, String _password) {
        if (!_email.isEmpty() || !_password.isEmpty()) {
            mAuthHelper.userSignIn(_email, _password, this);
            getView().showProgressDialog();
        } else {
            getView().showInputError();
        }
    }

    /**
     * Calls the views signInViaGoogle method and passes the GoogleApiClient instance.
     */
    public void signInWithGoogle() {
        getView().signInViaGoogle(mAuthHelper.getGoogleApiClient());
    }

    /**
     * Handles the sign in result of the Google sign in intent of LoginActivity.
     *
     * @param _result the result as GoogleSignInResult
     * @see com.google.android.gms.auth.api.signin.GoogleSignInResult
     */
    public void handleSignInResult(GoogleSignInResult _result) {
        getView().showProgressDialog();
        mAuthHelper.googleSignIn(_result, this);
    }

    /**
     * Set the GoogleApiClient instance of the AuthenticationHelperImpl instance
     *
     * @param _googleApiClient the GoogleApiClient instance
     * @see com.google.android.gms.common.api.GoogleApiClient
     * @see eu.codingtub.wandershot.api.authentication.AuthenticationHelperImpl
     */
    public void setGoogleApiClient(GoogleApiClient _googleApiClient) {
        mAuthHelper.setGoogleApiClient(_googleApiClient);
    }

    /**
     * Callback method of the onRequestListener, is called when the sign in operation was successful.
     * Stops the ProgressDialog running on the view and calls the view's logInSuccessful method.
     *
     * @see OnAuthenticationListener
     */
    @Override
    public void onUserSignedIn() {
        getView().stopProgressDialog();
        getView().logInSuccessful();
    }

    /**
     * Callback method of the onRequestListener, is called when the sign in operation failed.
     * Stops the ProgressDialog running on the view and passes the error to the view's logInFailed
     * method.
     *
     * @param _exception the error that occurred while performing the action as String
     * @see OnAuthenticationListener
     */
    @Override
    public void onSignInFailed(String _exception) {
        getView().stopProgressDialog();
        getView().logInFailed(_exception);
    }

    @Override
    public void onGoogleSignInSuccess(String _name, String _email, String _country, String _birthday, String _profilePicturePath) {
        mDatabaseHelper.initUser(mAuthHelper.getCurrentUser(), _name, _email, _country, _birthday, _profilePicturePath, true);
        getView().stopProgressDialog();
        getView().logInSuccessful();
    }

    @Override
    public void onGoogleSignInFailed(String _exception) {
        getView().stopProgressDialog();
        getView().logInFailed("");
    }
}
