package eu.codingtub.wandershot.api.camera;

import java.io.File;

/**
 * This class acts as a callback listener used in the file saving process.
 *
 * @author Michael Rieger
 */

public interface OnImageSavedListener {
    /**
     * Is called when an action was successful
     *
     * @param _file the saved File
     * @see java.io.File
     */
    void onImageSaved(File _file);

    /**
     * Is called when an action failed
     *
     * @param _exception the error that occurred while performing the action as String
     * @see java.lang.String
     */
    void onImageSaveFailed(String _exception);
}
