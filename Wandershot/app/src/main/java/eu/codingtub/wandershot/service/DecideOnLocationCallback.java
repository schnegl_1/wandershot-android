package eu.codingtub.wandershot.service;

import eu.codingtub.wandershot.models.Snapspot;

/**
 * Interface which acts as a Callback for Classes using the methods of the LocationService
 *
 * @author Martin Schneglberger
 * @see LocationService
 */

public interface DecideOnLocationCallback {

    void hideInsideSnapspotButton(boolean _shouldHide);

    /**
     * Gets called from the LocationHelper if the user currently is nearby of multiple Snapspots
     * Shows a Notification if the app runs in background.
     */
    void showNotficationInsideSnapspot(Snapspot _snapspot);


    /**
     * Gets called from the LocationHelper if the user currently is nearby of multiple Snapspots
     * Shows a Notification if the app runs in background.
     */
    void showNotficationMultipleSnapspotsNearby(int _snapspotsNearby);

    /**
     * Gets called from the LocationHelper if the user currently is nearby of a Snapspots.
     * Shows a Notification if the app runs in background.
     */
    void showNotficationSingleSnapspotNearby();
}
