package eu.codingtub.wandershot.api.location;

import android.location.Location;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.service.DecideOnLocationCallback;
import eu.codingtub.wandershot.utils.FirebaseConstants;

/**
 * This class handles database communication to receive snapspots from the Firebase-Database and
 * the general management of Snapspots.
 *
 * @author Martin Schneglberger
 * @see Snapspot
 */

public class LocationHelperImpl implements LocationHelper {

    private final static int NEARBY_BOUNDRY_IN_M = 50000;
    private final static int NEARBY_REFRESH_DISTANCE_IN_M = 5000;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mLocationDatabaseRef;

    private ArrayList<Snapspot> mSnapspots;
    private ArrayList<Snapspot> mSnapspotsNearby = null;

    private LatLng mPositionOfSnapspotsNearby = null;

    private boolean mShowSnapspotButtonHidden = true;

    private Snapspot mCurrentSnapspot = null;

    /**
     * Constructor
     *
     * @param _firebaseDatabase Reference to the FirebaseDatabase
     * @see FirebaseDatabase
     */
    public LocationHelperImpl(FirebaseDatabase _firebaseDatabase) {
        mFirebaseDatabase = _firebaseDatabase;

    }

    /**
     * Loads all Snapspots from the Firebase-Database into an ArrayList
     *
     * @param _listener OnSnapspotReceivedListender to provide callbacks
     * @see OnSnapspotsReceivedListener
     * @see Snapspot
     */
    @Override
    public void getSnapspots(final OnSnapspotsReceivedListener _listener) {
        mLocationDatabaseRef = mFirebaseDatabase.getReference(FirebaseConstants.DATABASE_SNAPSPOTS);

        mSnapspots = new ArrayList<>();

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot _dataSnapshots) {
                for (DataSnapshot snapshot : _dataSnapshots.getChildren()) {
                    mSnapspots.add(snapshot.getValue(Snapspot.class));
                }
                _listener.onSnapspotsReceived(mSnapspots);
            }

            @Override
            public void onCancelled(DatabaseError _databaseError) {
                _listener.onSnapspotsReceiveFailed();
            }
        };
        mLocationDatabaseRef.addListenerForSingleValueEvent(eventListener);
    }

    /**
     * Gets called from the GPS-Service whenever a LocationUpdate is received.
     * Calculates the exact distance to nearby Snapspots and invoke the appropriate method
     * if given reason
     *
     * @param _location Location-object containing the new Location
     * @param _callback DecideOnLocationCallback to provide decision-based result-callbacks
     * @see Location
     * @see eu.codingtub.wandershot.service.LocationService
     * @see DecideOnLocationCallback
     */
    @Override
    public void onLocationChanged(Location _location, DecideOnLocationCallback _callback) {
        LatLng latlng = new LatLng(_location.getLatitude(), _location.getLongitude());

        getSnapspotsNearby(latlng);

        Snapspot result = checkIfInsideSnapspot(latlng);
        if (result != null) {
            mCurrentSnapspot = result;
            if (mShowSnapspotButtonHidden) {
                _callback.hideInsideSnapspotButton(false);
                mShowSnapspotButtonHidden=false;
            }
            _callback.showNotficationInsideSnapspot(result);
        } else if (!App.get().isHomeFragmentInForeground()) {
            mCurrentSnapspot = null;
            if (!mShowSnapspotButtonHidden) {
                _callback.hideInsideSnapspotButton(true);
                mShowSnapspotButtonHidden = true;
            }
            int snapspotsNearby = getInsideTriggerPointSnapspots(latlng);
            if (snapspotsNearby == 1) {
                _callback.showNotficationSingleSnapspotNearby();
            } else if (snapspotsNearby > 1) {
                _callback.showNotficationMultipleSnapspotsNearby(snapspotsNearby);
            }
        }
    }

    /**
     * Calculates the distance between two locations
     *
     * @param _point1 LatLng of first position
     * @param _point2 LatLng of second position
     * @return distance as a float
     */
    @Override
    public float getDistanceBetweenPoints(LatLng _point1, LatLng _point2) {
        float result[] = new float[3];
        Location.distanceBetween(_point1.latitude, _point1.longitude, _point2.latitude, _point2
                .longitude, result);
        return result[0];
    }

    /**
     * Get all the Snapspots nearby the user. This will only happen if not loaded yet or if the user
     * has moved a certain distance ({@value NEARBY_REFRESH_DISTANCE_IN_M})
     *
     * @param _currentLocation LatLng containing the current location of the user
     * @see LatLng
     */
    @Override
    public void getSnapspotsNearby(LatLng _currentLocation) {

        if (mPositionOfSnapspotsNearby == null ||
                getDistanceBetweenPoints(mPositionOfSnapspotsNearby, _currentLocation) >
                        NEARBY_REFRESH_DISTANCE_IN_M) {
            mSnapspotsNearby = new ArrayList<>();

            for (Snapspot temp : mSnapspots) {
                float distanceToTriggerPoint = temp.getDistanceToTriggerPoint(_currentLocation);
                if (distanceToTriggerPoint <= NEARBY_BOUNDRY_IN_M) {
                    mSnapspotsNearby.add(temp);
                }
            }

            mPositionOfSnapspotsNearby = _currentLocation;
        }
    }

    /**
     * Checks if the user is inside of a Snapspot
     *
     * @param _currentLocation LatLng containing current location of the user
     * @return Snapspot if user is inside of a Snapspot, null otherwise
     */
    @Nullable
    @Override
    public Snapspot checkIfInsideSnapspot(LatLng _currentLocation) {
        Snapspot result = null;

        for (Snapspot temp : mSnapspotsNearby) {
            if (temp.getDistanceToSnapspot(_currentLocation) <= 0) {
                result = temp;
                break;
            }
        }

        return result;
    }

    /**
     * Checks is the user close enough to a Snapspot to trigger a Notification
     * <p>
     * This only happens once for every Snapspot
     * </p>
     *
     * @param _currentLocation Current location of the user as LatLng
     * @return int representing the number of snapspots which should trigger a Notification
     */
    @Override
    public int getInsideTriggerPointSnapspots(LatLng _currentLocation) {
        int snapspotsNearbyCount = 0;

        for (Snapspot temp : mSnapspotsNearby) {
            if (!temp.isNotificationNearbyTriggered()) {
                float distanceToTriggerPoint = temp.getDistanceToTriggerPoint(_currentLocation);
                float distanceToSnapspot = distanceToTriggerPoint + temp.getTriggerDistance()
                        .getDistanceInM();
                if (distanceToTriggerPoint <= 0 && distanceToSnapspot > 0) {
                    snapspotsNearbyCount++;
                    temp.setNotificationNearbyTriggered(true);
                }
            }
        }

        return snapspotsNearbyCount;
    }

    /**
     * Returns the Snapspots fetched from the database
     *
     * @return ArrayList of Snapspots
     * @see Snapspot
     * @see #getSnapspots(OnSnapspotsReceivedListener)
     */
    public ArrayList<Snapspot> getSnapspotsList() {
        return mSnapspots;
    }

    /**
     * Returns the snapspot the user is currently in.
     *
     * @return Snapspot the user is in, null if outside of a snapspot
     */
    @Override
    public Snapspot getCurrentSnapspot() {
        return mCurrentSnapspot;
    }

    /**
     * Return the name of a snapspot by the StorageReference of a uploaded picture inside of the
     * Snapspot
     *
     * @param _reference StorageReference to a image inside of a Snapspot
     * @return Name of the Snapspot which contains this image, "Not Found" if not found
     *
     * @see StorageReference
     */
    public String getSnapspotsNameByReference(StorageReference _reference){
        // TODO: 08/02/2017 This is a really ugly hotfix. Please...PLEASE delete before releasing
        //instead of disgusting the next ten generations of yours

        String tempStr = _reference.toString();
        String[] tempArr = tempStr.split("/");
        String snapId = tempArr[4];

        for(Snapspot temp : mSnapspots){
            if(temp.getID()== Integer.valueOf(snapId)){
                return temp.getName();
            }
        }

        return "Not Found";
    }
}
