package eu.codingtub.wandershot.api.listener;

/**
 * This class acts as a callback listener used with database interaction.
 *
 * @author Michael Rieger
 */

public interface OnAuthenticationListener {

    /**
     * Is called when an action was successful
     */
    void onUserSignedIn();

    /**
     * Is called when an action failed
     *
     * @param _exception the error that occurred while performing the action as String
     * @see java.lang.String
     */
    void onSignInFailed(String _exception);
}
