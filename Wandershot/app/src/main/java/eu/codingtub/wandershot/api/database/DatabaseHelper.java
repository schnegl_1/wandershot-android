package eu.codingtub.wandershot.api.database;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.StorageReference;

import eu.codingtub.wandershot.api.listener.OnSnapUploadListener;
import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.api.listener.OnUserDataListener;
import eu.codingtub.wandershot.api.listener.OnWandershotUsersReceivedListener;
import eu.codingtub.wandershot.models.Snapspot;

/**
 * Provides methods that will be used in the process of database data.
 *
 * @author Michael Rieger
 */

public interface DatabaseHelper {

    /**
     * Initializes a user object represented by the following attributes.
     *
     * @param _currentUser        the current user as FirebaseUser
     * @param _name               the user's name as String
     * @param _email              the user's email address as String
     * @param _country            the user's country as String
     * @param _birthday           the user's birth date as String
     * @param _profilePicturePath the path of the user's profile picture on the phone as String
     * @see java.lang.String
     * @see com.google.firebase.auth.FirebaseUser
     */
    void initUser(FirebaseUser _currentUser, String _name, String _email, String _country,
                  String _birthday, String _profilePicturePath, boolean _isGoogleSignIn);

    /**
     * Uploads the user's profile picture from the given file path
     *
     * @param _currentUser the current user as FirebaseUser
     * @param _path        the path of a picture on the phone as String
     * @see java.lang.String
     * @see com.google.firebase.auth.FirebaseUser
     */
    void uploadProfileImage(FirebaseUser _currentUser, String _path);

    /**
     * Uploads the user's profile picture from the given file path
     *
     * @param _currentUser the current user as FirebaseUser
     * @param _path        the path of a picture on the phone as String
     * @param _snapspot    the Snapspot where the snap was taken
     * @param _listener    the callback listener
     * @see java.lang.String
     * @see com.google.firebase.auth.FirebaseUser
     * @see eu.codingtub.wandershot.models.Snapspot
     * @see OnSnapUploadListener
     */
    void uploadSnap(FirebaseUser _currentUser, String _path, Snapspot _snapspot,
                    OnSnapUploadListener _listener);

    /**
     * Fetches all data of a specific user.
     *
     * @param _currentUser the currently signed in user
     * @param _listener    the callback listener
     * @see com.google.firebase.auth.FirebaseUser
     * @see OnUserDataListener
     */
    void getUserData(FirebaseUser _currentUser, OnUserDataListener _listener);

    /**
     * Returns all children of the users snapspots property.
     *
     * @param _currentUser the currently signed in user
     * @param _listener    the callback listener
     * @see com.google.firebase.auth.FirebaseUser
     * @see OnSnapspotsReceivedListener
     */
    void getUserSnapspots(FirebaseUser _currentUser, OnSnapspotsReceivedListener _listener);

    /**
     * Returns all children of the snapspot users property.
     *
     * @param _currentSnapspot the Snapspot the user is currently in
     * @param _listener        the callback listener
     * @see com.google.firebase.auth.FirebaseUser
     * @see OnSnapspotsReceivedListener
     */
    public void getSnapspotUsers(Snapspot _currentSnapspot, OnWandershotUsersReceivedListener
            _listener);

    /**
     * Returns a ArrayList of WandershotUsers which uploaded a Snap inside a certain Snapspot, which
     * is referenced to by a StorageReference pointing to a snap of this snapspot
     *
     * @param _reference StorageReference to a image taken inside of a snapspot
     * @param _listener the callback listener
     *
     * @see StorageReference
     */
    public void getUserNameByReference(StorageReference _reference,
                                         final OnWandershotUsersReceivedListener _listener);

}
