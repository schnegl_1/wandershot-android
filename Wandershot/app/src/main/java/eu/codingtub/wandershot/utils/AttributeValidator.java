package eu.codingtub.wandershot.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Michael on 15.01.2017.
 */

public class AttributeValidator {

    public static boolean isEmail(String _emailString) {
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(_emailString);

        return m.matches();
    }
}
