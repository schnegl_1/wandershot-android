package eu.codingtub.wandershot.ui.feed;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.api.listener.OnWandershotUsersReceivedListener;
import eu.codingtub.wandershot.models.WandershotUser;
import eu.codingtub.wandershot.utils.FirebaseConstants;
import eu.codingtub.wandershot.utils.SnapspotCategory;

/**
 * The Adapter of the RecyclerView used in the FeedFragment.
 * Binds the ViewHolders for all items and is responsible for downloading and caching images.
 * <p>
 * Defines an Interface used for tunneling long-presses on an image of an item to the Fragment.
 * </p>
 *
 * @author Martin Schneglberger
 * @see android.support.v7.widget.RecyclerView.Adapter
 * @see FeedViewHolder
 * @see OnFeedItemLongClickListener
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedViewHolder> {

    private ArrayList<StorageReference> mImageReferences;
    private String mType;
    private Fragment mParentFragment;
    private OnFeedItemLongClickListener mLongClickListener;

    /**
     * Constructor
     *
     * @param _imageReferences ArrayList of StorageReferences which define the ReyclerView-items
     * @param _parentFragment  Parent-Fragement. Needed for receiving Context in this class
     * @param _listener        OnFeedItemLongClickListener for tunneling long-presses on images
     * @param _type            Type of this Feed (Snapspot-Feed or User-Feed)
     *                         {@link FeedFragment#FEED_TYPE_SNAPSPOT} and
     *                         {@link FeedFragment#FEED_TYPE_USER}
     * @see StorageReference
     * @see Fragment
     * @see OnFeedItemLongClickListener
     */
    FeedAdapter(ArrayList<StorageReference> _imageReferences, Fragment _parentFragment,
                OnFeedItemLongClickListener _listener, String _type) {
        mImageReferences = _imageReferences;
        mParentFragment = _parentFragment;
        mLongClickListener = _listener;
        mType = _type;
    }

    /**
     * Interface used for tunneling long-presses on an image of an item to the Fragment.
     */
    public interface OnFeedItemLongClickListener {
        void onItemLongClickListener(FeedViewHolder _viewHolder);
    }

    /**
     * Called when RecyclerView needs a new FeedViewHolder to represent an item.
     *
     * @param _parent   the ViewGroup into which the new View will be added after it is
     *                  bound to an adapter position.
     * @param _viewType int The view type of the new View.
     * @return A new FeedViewHolder that holds a View of the given view type.
     * @see FeedViewHolder
     * @see ViewGroup
     */
    @Override
    public FeedViewHolder onCreateViewHolder(ViewGroup _parent, int _viewType) {
        View itemView = LayoutInflater.from(_parent.getContext()).inflate(R.layout.feed_item,
                _parent, false);

        return new FeedViewHolder(itemView);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * Loads Snaps into the ImageView via Glide. While loading a ProgressBar is displayed.
     * <p>
     * Information about the Snap like the name of the creator, the creation date and the
     * according snapspot are fetched via the MetaData of the Image.
     * </p>
     *
     * @param _holder   The FeedViewHolder which should be updated and is responsible for
     *                  displaying the data of the item
     * @param _position int The position of the item within the adapter's data set.
     * @see FeedViewHolder
     * @see StorageReference
     * @see Glide
     * @see android.widget.ProgressBar
     * @see StorageMetadata
     */
    @Override
    public void onBindViewHolder(final FeedViewHolder _holder, int _position) {

        final StorageReference referenceOfThisImage = mImageReferences.get(_position);

        if (mType.equals(FeedFragment.FEED_TYPE_SNAPSPOT)) {
            setProfilePictureInViewHolder(referenceOfThisImage, _holder);
            App.get().getDatabaseHelper().getUserNameByReference(referenceOfThisImage,
                    new OnWandershotUsersReceivedListener() {
                @Override
                public void onWandershotUsersReceived(ArrayList<WandershotUser> _wandershotUsers) {
                    String feedItemText = _wandershotUsers.get(0).getName();
                    _holder.mFeedItemText.setText(feedItemText);
                }

                @Override
                public void onWandershotUsersReceivedFailed() {
                    String feedItemText = "User not found";
                    _holder.mFeedItemText.setText(feedItemText);
                }
            });
        } else{
            setIconOfFeedViewHolder(referenceOfThisImage, _holder);
            String feedItemText = App.get().getLocationHelper()
                    .getSnapspotsNameByReference(referenceOfThisImage);
            _holder.mFeedItemText.setText(feedItemText);
        }
        //Load Snap into ImageView
        Glide.with(mParentFragment).using(new FirebaseImageLoader())
                .load(referenceOfThisImage)
                .listener(new RequestListener<StorageReference, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, StorageReference model, Target
                            <GlideDrawable> target, boolean isFirstResource) {
                        _holder.mFeedItemText.setText(R.string.feed_error_text);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, StorageReference
                            model, Target<GlideDrawable> target, boolean isFromMemoryCache,
                                                   boolean isFirstResource) {
                        _holder.mFeedItemProgressBar.setVisibility(View.GONE);
                        //Hide ProgressBar
                        return false;
                    }
                })
                .into(_holder.mFeedItemImage);

        //Read information about the snap from the metadata
        referenceOfThisImage.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata _storageMetadata) {

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                long timeStamp = _storageMetadata.getCreationTimeMillis();
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(timeStamp);

                String timeAsString = formatter.format(calendar.getTime());
                _holder.mFeedItemDate.setText(timeAsString);
            }
        });

        _holder.setStorageReference(referenceOfThisImage);
        _holder.setUpLongClickListener(mLongClickListener);
    }

    /**
     * Loads the user's profile-picture into the ReyclerView-item.
     * Image gets cropped into circular-format.
     * Used for Snapspot-Feeds.
     *
     * @param _reference StorageReference of the Snap-Image (UserID gets extracted)
     * @param _holder    Current FeedViewHolder
     * @see StorageReference
     * @see FeedViewHolder
     * @see Glide
     */
    private void setProfilePictureInViewHolder(StorageReference _reference, final FeedViewHolder
            _holder) {
        //BitmapImageViewTarget is needed to crop the image circular
        Glide.with(mParentFragment).using(new FirebaseImageLoader())
                .load(getProfilePictureReference(_reference))
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(_holder.mFeedItemProfilePic) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(mParentFragment.getResources
                                        (), resource);
                        circularBitmapDrawable.setCircular(true);
                        _holder.mFeedItemProfilePic.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    /**
     * Loads the category-icon of a snapspot into the ReyclerView-item.
     * Image gets cropped into circular-format.
     * Used for User-Feeds
     *
     * @param _reference StorageReference of the Snap-Image
     * @param _holder    Current FeedViewHolder
     * @see StorageReference
     * @see FeedViewHolder
     * @see Glide
     */
    private void setIconOfFeedViewHolder(StorageReference _reference, final FeedViewHolder
            _holder) {
        //TODO: GET CATEGORY
        if (mParentFragment != null && mParentFragment.getActivity() != null) {
            Drawable drawable = mParentFragment.getActivity().getDrawable(SnapspotCategory.LANDSCAPE
                    .getResourceIcon());
            _holder.mFeedItemProfilePic.setImageDrawable(drawable);
        }
    }

    /**
     * Converts a StorageReference of a Snap into a StorageReference for the profile-picture
     *
     * @param _referenceOfThisImage StorageReference to the snap
     * @return StorageReference to the users profile-picture who created the snap
     */
    private StorageReference getProfilePictureReference(StorageReference _referenceOfThisImage) {
        //Snap references have the path snaps/snapID/userID
        StorageReference result = FirebaseStorage.getInstance().getReference(FirebaseConstants
                .STORAGE_PROFILE_PICTURE);

        String referenceAsString = _referenceOfThisImage.toString();
        String userIdAsString = referenceAsString.substring(referenceAsString.lastIndexOf("/") + 1);

        result = result.child(userIdAsString);

        return result;
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return number of items in the data set of the adapter
     */
    @Override
    public int getItemCount() {
        return mImageReferences.size();
    }
}
