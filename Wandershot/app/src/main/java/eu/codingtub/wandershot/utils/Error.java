package eu.codingtub.wandershot.utils;

/**
 * An enum class for errors that can occur.
 *
 * @author Michael Rieger
 */

public enum Error {
    NAME_EMPTY(1, "Please enter a name."),
    EMAIL_EMPTY(2, "Please enter an email address."),
    PASSWORD_EMPTY(3, "Please enter a password."),
    COUNTRY_EMPTY(4, "Please enter a country."),
    BIRTHDAY_EMPTY(5, "Please enter a birthday."),
    NO_GPS(6, "Please enable GPS for Wandershot to work");

    private final int mCode;
    private final String mDescription;

    /**
     * Constructor
     *
     * @param _code        the error code as int
     * @param _description the error description as String
     * @see java.lang.String
     */
    Error(int _code, String _description) {
        mCode = _code;
        mDescription = _description;
    }

    /**
     * The toString method of this class. Returns the error description.
     *
     * @return the error description as String
     * @see java.lang.String
     */
    @Override
    public String toString() {
        return mDescription;
    }

    // Getter / Setter
    public int getCode() {
        return mCode;
    }

    public String getDescription() {
        return mDescription;
    }
}
