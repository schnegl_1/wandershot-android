package eu.codingtub.wandershot.api.listener;


import eu.codingtub.wandershot.models.Snapspot;

/**
 * This class acts as a callback listener used with database interaction.
 *
 * @author Michael Rieger
 */

public interface OnSnapUploadListener {

    /**
     * Is called when an action was successful
     */
    void onSnapUploaded(Snapspot _snapspot);

    /**
     * Is called when an action failed
     *
     * @param _exception the error that occurred while performing the action as String
     * @see java.lang.String
     */
    void OnSnapUploadFailed(String _exception);
}
