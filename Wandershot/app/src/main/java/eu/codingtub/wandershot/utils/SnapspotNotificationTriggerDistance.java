package eu.codingtub.wandershot.utils;

/**
 * A enum used to specify how close a user has to come up to a snapspot until a notification gets
 * triggered
 *
 * @author Martin Schneglberger
 * @see eu.codingtub.wandershot.models.Snapspot
 */

public enum SnapspotNotificationTriggerDistance {
    WIDE(1, 7000),
    MIDDLE(2, 5000),
    SHORT(3, 2000),
    CITY(4, 400);

    private final int mCode;
    private final int mDistanceInM;

    /**
     * Constructor
     *
     * @param _code        int holding the code of this TriggerDistance
     * @param _distanceInM int holding the TriggerDistance in meters
     */
    SnapspotNotificationTriggerDistance(int _code, int _distanceInM) {
        mCode = _code;
        mDistanceInM = _distanceInM;
    }

    /**
     * Returns the TriggerDistance in meters
     *
     * @return int holding the TriggerDistance in meters
     */
    public int getDistanceInM() {
        return mDistanceInM;
    }
}
