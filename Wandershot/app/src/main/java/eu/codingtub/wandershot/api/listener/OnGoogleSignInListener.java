package eu.codingtub.wandershot.api.listener;

/**
 * This class acts as a callback listener used for the google sign in option.
 *
 * @author Michael Rieger
 */

public interface OnGoogleSignInListener {

    /**
     * Is called when an action was successful
     * Parameters should later be handed over to the implemented method handling further database
     * related tasks.
     *
     * @param _name               the user's name as String
     * @param _email              the user's email address as String
     * @param _country            the user's country as String
     * @param _birthday           the user's birthday as String
     * @param _profilePicturePath the path of the user's profile picture as String
     * @see java.lang.String
     */
    void onGoogleSignInSuccess(String _name, String _email, String _country, String _birthday,
                               String _profilePicturePath);

    /**
     * Is called when an action failed
     *
     * @param _exception the error that occurred while performing the action as String
     * @see java.lang.String
     */
    void onGoogleSignInFailed(String _exception);
}
