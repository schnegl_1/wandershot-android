package eu.codingtub.wandershot.api.listener;

import java.util.ArrayList;

import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.models.WandershotUser;

/**
 * This interface acts as a callback for methods fetching a list of WandershotUsers from the
 * database.
 *
 * @author Martin Schneglberger
 * @see eu.codingtub.wandershot.api.database.DatabaseHelperImpl
 * @see Snapspot
 */

public interface OnWandershotUsersReceivedListener {

    /**
     * Is called when an action was successful
     */
    void onWandershotUsersReceived(ArrayList<WandershotUser> _wandershotUsers);

    /**
     * Is called when an action failed
     */
    void onWandershotUsersReceivedFailed();
}
