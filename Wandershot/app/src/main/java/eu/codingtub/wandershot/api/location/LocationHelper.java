package eu.codingtub.wandershot.api.location;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.service.DecideOnLocationCallback;

/**
 * This interface defines the methods used in the LocationHelper
 *
 * @author Martin Schneglberger
 * @see LocationHelperImpl
 */

public interface LocationHelper {
    int LOCATION_CHECK_INTERVAL_MILLI = 10000;      //Minimum update-interval
    int FASTEST_LOCATION_CHECK_INTERVAL_MILLI = 5000;   //Highes processable update-interval

    /**
     * Loads all Snapspots from the Firebase-Database into an ArrayList
     *
     * @param _listener OnSnapspotReceivedListender to provide callbacks
     * @see OnSnapspotsReceivedListener
     * @see Snapspot
     */
    void getSnapspots(final OnSnapspotsReceivedListener _listener);

    /**
     * Gets called from the GPS-Service whenever a LocationUpdate is received.
     * Calculates the exact distance to nearby Snapspots and invoke the appropriate method
     * if given reason
     *
     * @param _location Location-object containing the new Location
     * @param _callback DecideOnLocationCallback to provide decision-based result-callbacks
     * @see Location
     * @see eu.codingtub.wandershot.service.LocationService
     * @see DecideOnLocationCallback
     */
    void onLocationChanged(Location _location, DecideOnLocationCallback _callback);

    /**
     * Calculates the distance between two locations
     *
     * @param _point1 LatLng of first position
     * @param _point2 LatLng of second position
     * @return distance as a float
     */
    float getDistanceBetweenPoints(LatLng _point1, LatLng _point2);

    /**
     * Get all the Snapspots nearby the user. This will only happen if not loaded yet or if the user
     * has moved a certain distance ({@value NEARBY_REFRESH_DISTANCE_IN_M})
     *
     * @param _currentLocation LatLng containing the current location of the user
     * @see LatLng
     */
    void getSnapspotsNearby(LatLng _currentLocation);

    /**
     * Checks if the user is inside of a Snapspot
     *
     * @param _currentLocation LatLng containing current location of the user
     * @return Snapspot if user is inside of a Snapspot, null otherwise
     */
    Snapspot checkIfInsideSnapspot(LatLng _currentLocation);

    /**
     * Checks is the user close enough to a Snapspot to trigger a Notification
     * <p>
     * This only happens once for every Snapspot
     * </p>
     *
     * @param _currentLocation Current location of the user as LatLng
     * @return int representing the number of snapspots which should trigger a Notification
     */
    int getInsideTriggerPointSnapspots(LatLng _currentLocation);

    /**
     * Returns the Snapspots fetched from the database
     *
     * @return ArrayList of Snapspots
     * @see Snapspot
     * @see #getSnapspots(OnSnapspotsReceivedListener)
     */
    ArrayList<Snapspot> getSnapspotsList();

    /**
     * Returns the snapspot the user is currently in.
     *
     * @return Snapspot the user is in, null if outside of a snapspot
     */
    Snapspot getCurrentSnapspot();

    /**
     * Return the name of a snapspot by the StorageReference of a uploaded picture inside of the
     * Snapspot
     *
     * @param _reference StorageReference to a image inside of a Snapspot
     * @return Name of the Snapspot which contains this image, "Not Found" if not found
     *
     * @see StorageReference
     */
    public String getSnapspotsNameByReference(StorageReference _reference);
}
