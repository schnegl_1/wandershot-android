package eu.codingtub.wandershot.ui.feed;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.codingtub.wandershot.App;
import eu.codingtub.wandershot.R;
import eu.codingtub.wandershot.api.listener.OnSnapspotsReceivedListener;
import eu.codingtub.wandershot.api.listener.OnWandershotUsersReceivedListener;
import eu.codingtub.wandershot.api.database.DatabaseHelper;
import eu.codingtub.wandershot.api.location.LocationHelper;
import eu.codingtub.wandershot.models.Snapspot;
import eu.codingtub.wandershot.models.WandershotUser;
import eu.codingtub.wandershot.presenter.FeedPresenter;
import eu.codingtub.wandershot.utils.FirebaseConstants;

/**
 * This Fragment represents the "Feed"-Screen of the Application.
 * <p>
 * It shows a Recycler-View containing all the images of either a snapspot or the user with
 * additional information.
 * By long-pressing a image the user can download it into his external storage / gallery.
 * <p>
 * The content of the items are defined through an ArrayList of StorageReferences.
 * </p>
 *
 * @author Martin Schneglberger
 * @see FeedAdapter
 * @see FeedViewHolder
 * @see RecyclerView
 * @see StorageReference
 */

public class FeedFragment extends MvpFragment<FeedView, FeedPresenter> implements
        FeedAdapter.OnFeedItemLongClickListener {

    @BindView(R.id.feed_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.feed_toolbar)
    Toolbar mToolbar;

    public static final String TAG = "FeedFragment";
    public static final String FEED_TYPE_KEY = "feedType";
    public static final String FEED_TYPE_USER = "userFeed";
    public static final String FEED_TYPE_SNAPSPOT = "snapspotFeed";
    private static final int FEED_DOWNLOAD_BTN_ID = 456;

    private static final int REQUEST_WRITE_PERMISSION_CODE = 5555;

    private DatabaseHelper mDatabaseHelper;
    private LocationHelper mLocationHelper;
    private String mType;
    private StorageReference mStorageReference;
    private StorageReference mSelectedImageReference;
    private ArrayList<StorageReference> mImageReferences;

    @Override
    public FeedPresenter createPresenter() {
        return new FeedPresenter();
    }

    /**
     * The fragments onCreateView-method.
     * Initializes the Recycler-View.
     *
     * @param _inflater           LayoutInflater object that can be used to inflate any views in the fragment
     * @param _container          In non-null, this is the parent view the UI should be attached to
     * @param _savedInstanceState If non-null, this fragment is being re-constructed from a
     *                            previous saved state
     * @return View-hierarchy associated with the fragment
     */
    @Override
    public View onCreateView(LayoutInflater _inflater, ViewGroup _container, Bundle
            _savedInstanceState) {

        View v = _inflater.inflate(R.layout.fragment_feed, _container, false);

        mDatabaseHelper = App.get().getDatabaseHelper();
        mLocationHelper = App.get().getLocationHelper();

        Bundle args = getArguments();
        mType = args.getString(FEED_TYPE_KEY);

        mStorageReference = FirebaseStorage.getInstance().getReference(FirebaseConstants.STORAGE_SNAPS);

        ButterKnife.bind(this, v);

        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Feed");

        if (mType.equals(FEED_TYPE_SNAPSPOT)) {
            getSnapspotImageUrls();
        } else {
            getUserImageUrls();
        }

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return v;
    }

    /**
     * Fills the ArrayList {@link #mImageReferences} of ImageReferences with the
     * StorageReferences to the images of the desired Snapspot.
     *
     * @see StorageReference
     */
    private void getSnapspotImageUrls() {
        final Snapspot currentSpot = mLocationHelper.getCurrentSnapspot();

        if (currentSpot != null) {
            final ArrayList<WandershotUser> users = new ArrayList<>();
            mImageReferences = new ArrayList<>();

            mDatabaseHelper.getSnapspotUsers(currentSpot, new OnWandershotUsersReceivedListener() {
                @Override
                public void onWandershotUsersReceived(ArrayList<WandershotUser> _wandershotUsers) {
                    users.addAll(_wandershotUsers);

                    String snapspotID = String.valueOf(currentSpot.getID());
                    for (WandershotUser user : users) {
                        String userID = user.getUserId();
                        String relReference = snapspotID + "/" + userID;
                        StorageReference reference = mStorageReference.child(relReference);
                        mImageReferences.add(reference);
                    }

                    createReyclerView();
                }

                @Override
                public void onWandershotUsersReceivedFailed() {
                    Log.e(TAG, "Couldn't get the snapspot's users");
                }
            });
        }
    }

    /**
     * Fills the ArrayList {@link #mImageReferences} of ImageReferences with the
     * StorageReferences to the images of the desired User.
     *
     * @see StorageReference
     */
    private void getUserImageUrls() {
        final FirebaseUser thisUser = FirebaseAuth.getInstance().getCurrentUser();
        final ArrayList<Snapspot> userSnapspots = new ArrayList<>();
        mImageReferences = new ArrayList<>();

        mDatabaseHelper.getUserSnapspots(thisUser, new
                OnSnapspotsReceivedListener() {
                    @Override
                    public void onSnapspotsReceived(ArrayList<Snapspot> _snapspots) {
                        userSnapspots.addAll(_snapspots);

                        String userID = thisUser.getUid();
                        for (Snapspot snapspot : userSnapspots) {
                            String snapspotID = String.valueOf(snapspot.getID());
                            String relReference = snapspotID + "/" + userID;
                            StorageReference reference = mStorageReference.child(relReference);
                            mImageReferences.add(reference);
                        }

                        createReyclerView();
                    }

                    @Override
                    public void onSnapspotsReceiveFailed() {
                        Log.e(TAG, "Couldn't get the users snapspots");
                    }
                });
    }

    private void createReyclerView() {
        FeedAdapter adapter = new FeedAdapter(mImageReferences, this, this, mType);
        mRecyclerView.setAdapter(adapter);

        if (mType.equals(FEED_TYPE_USER)) {
            registerForContextMenu(mRecyclerView);
        }
    }

    /**
     * Callback triggered when the user long-presses an item of the RecyclerView
     *
     * @param _viewHolder FeedViewHolder of the item the user longpressed
     * @see FeedViewHolder
     * @see ContextMenu
     */
    @Override
    public void onItemLongClickListener(FeedViewHolder _viewHolder) {
        mSelectedImageReference = _viewHolder.getStorageReference();
        mRecyclerView.showContextMenuForChild(_viewHolder.mFeedItemImage);
    }

    /**
     * Gets called when the ContextMenu opens (triggered by longpressing an item of the ReyclerView)
     * Adds a "Download"-Button to the ContextMenu
     *
     * @param _menu     ContextMenu
     * @param _v        The view for which the context menu is being built
     * @param _menuInfo Extra information about the item for which the context menu should be shown.
     */
    @Override
    public void onCreateContextMenu(ContextMenu _menu, View _v, ContextMenu.ContextMenuInfo
            _menuInfo) {
        _menu.add(0, FEED_DOWNLOAD_BTN_ID, 0, R.string.feed_download_image);
    }

    /**
     * Handles selections made on the ContextMenu
     * Requests the WRITE_EXTERNAL_STORAGE permission in case the user haven't granted it yet
     *
     * @param _item MenuItem the user selected
     * @return boolean indication un-/successful handling
     * @see ContextMenu
     */
    @Override
    public boolean onContextItemSelected(MenuItem _item) {
        switch (_item.getItemId()) {
            case FEED_DOWNLOAD_BTN_ID: {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_WRITE_PERMISSION_CODE);
                } else {
                    downloadFile();
                    return true;
                }
            }
            default: {
                Log.e(TAG, "Unexpected option in ContextMenu");
            }
        }
        return false;
    }

    /**
     * Callback triggered when the user has decided on the runtime-permission dialog for
     * WRITE_EXTERNAL_STORAGE.
     * Downloads selected Image if permission granted.
     *
     * @param _requestCode  the request code as int
     * @param _permissions  the permissions as String[]
     * @param _grantResults the granted permissions as int[]
     */
    @Override
    public void onRequestPermissionsResult(int _requestCode, String _permissions[], int[] _grantResults) {
        switch (_requestCode) {
            case REQUEST_WRITE_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (_grantResults.length > 0
                        && _grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile();
                } else {
                    Toast.makeText(getActivity(), "Damn it I need this permission!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

    /**
     * Checks if the device has an external storage.
     *
     * @return boolean which is true if external storage is available
     */
    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the external storage is only readable
     *
     * @return boolean which is true if external storage can only be read
     */
    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    /**
     * Downloads the selected image into a subdirectory of the device's "Pictures" directory.
     * Image-Name is the current Date and time
     *
     * @see File
     */
    private void downloadFile() {
        if (mSelectedImageReference != null && isExternalStorageAvailable() && !isExternalStorageReadOnly()) {
            Toast.makeText(getActivity(), "Downloading...", Toast.LENGTH_SHORT).show();

            File rootPath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    "Wandershot");
            if (!rootPath.exists()) {
                rootPath.mkdirs();
            }

            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
            Date now = new Date();
            String fileName = sdfDate.format(now);

            final File localFile = new File(rootPath, fileName + ".jpg");

            mSelectedImageReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask
                    .TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "File created: " + localFile.toString());
                    Toast.makeText(getActivity(), "File downloaded!", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "File not created: " + exception.toString());
                }
            });

        }
    }
}
